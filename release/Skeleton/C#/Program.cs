﻿using System;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SampleClient
{
    class SocketClient
    {
        public class StateObject
        {
            public Socket workSocket = null;
            public const int BufferSize = 512;
            public byte[] buffer = new byte[BufferSize];
        }


        public class AsynchronousClient
        {
            // The port number for the remote device.
            private const int port = 25252;
            private static byte[] response = new byte[256];


            //Your team name & team number
            private static int teamNumber = 1;
            private static String teamName = "SampleRunningAI";
            private static AI testai = new AI();

            private static void StartClient()
            {
                // Connect to a remote device.
                try
                {
                    /*
                    Console.Write("Please input team number");
                    teamNumber = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Please input team Name");
                    teamName = Console.ReadLine();
                    */

                    // Establish the remote endpoint for the socket.
                    IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                    
                    Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    // Connect to the remote endpoint.
                    client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);

                                        
                    // Convert teamNumber & teamName to byte[] format / teamNumber + teamName -> data[]
                    byte[] name = Encoding.UTF8.GetBytes(teamName);
                    byte[] data = new byte[1 + name.Length];
                    data[0] = (byte)teamNumber;
                    testai.setTeam(data[0]);
                    for (int i=0; i < name.Length; i++)
                    {
                        data[i + 1] = name[i];
                    }

                    Send(client, data);
                    Receive(client);


                    byte[] datas = new byte[0];
                    while (true)
                    {
                        String message = Console.ReadLine();
                        if (message != null)
                        {
                            byte[] mes = Encoding.UTF8.GetBytes(message);
                            if (mes.Length == 0) mes = Encoding.UTF8.GetBytes(" ");
                            //int len = 40;
                            //if (mes.Length < len) len = mes.Length;
                            datas = new byte[256];
                            datas[0] = (byte)255;
                            for (int i = 0; i < mes.Length; i++)
                            {
                                datas[i + 1] = mes[i];
                            }
                            Send(client, datas);
                        }
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

            private static void ConnectCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.
                    Socket client = (Socket)ar.AsyncState;

                    // Complete the connection.
                    client.EndConnect(ar);

                    Console.WriteLine("Socket connected to {0}", client.RemoteEndPoint.ToString());

                    // Signal that the connection has been made.
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void Receive(Socket client)
            {
                try
                {
                    // Create the state object.
                    StateObject state = new StateObject();
                    state.workSocket = client;

                    // Begin receiving the data from the remote device.
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void ReceiveCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the state object and the client socket 
                    // from the asynchronous state object.
                    StateObject state = (StateObject)ar.AsyncState;
                    Socket client = state.workSocket;

                    int bytesRead = client.EndReceive(ar);

                    if (bytesRead > 0)
                    {
                        response = state.buffer;
                        if (bytesRead > 2)
                        {
                            SendData(client, state.buffer);
                        }
                    }
                    //state.buffer = new byte[StateObject.BufferSize];
                    Receive(client);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void Send(Socket client, byte[] data)
            {
                Console.WriteLine(data[0]);
                // Begin sending the data to the remote device.
                client.BeginSend(data, 0, data.Length, 0, new AsyncCallback(SendCallback), client);
            }

            private static void SendCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.
                    Socket client = (Socket)ar.AsyncState;

                    // Complete sending the data to the remote device.
                    int bytesSent = client.EndSend(ar);
                    Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }



            private static void SendData(Socket client, byte[] data)
            {
                Send(client, testai.main(data));                
            }




            public static void Main(String[] args)
            {
                StartClient();
                while (true) ;
            }

        }
    }



}