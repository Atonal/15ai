package org.haje.slimehopscotch.client;

import java.io.IOException;
import java.net.UnknownHostException;

public class Main {
	public static String HOST = "localhost";
	public static int PORT = 25252;
	public static String TEAM_NAME = "SampleJavaAI";
	
	public static int MAP_WIDTH = 25;
	public static int MAP_HEIGHT = 13;
	public static int SLIME_OUNT = 5;
	public static int TEAM_NUMBER = 1;
	public static AI testai = new AI();
	
	public static void main(String [] args) {
		try {
			ConnectHandler handler = new ConnectHandler(HOST, PORT);
			
			while(true) {
				if (testai.AllowConnection){
					byte[] data = handler.getNextTurn();
					handler.sendOrder(data);					
					
					// Sending Team Message
					if (AI.turn == 10) handler.sendMsg("내 실력을 제대로 보여줄 시간이군");	
					if (AI.turn >= 180) break;
				}
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
