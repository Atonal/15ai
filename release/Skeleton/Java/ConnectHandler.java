package org.haje.slimehopscotch.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

import com.sun.corba.se.impl.ior.ByteBuffer;

public class ConnectHandler {
	private final Socket sock;
	private final InputStream input;
	private final OutputStream output;
	
	public ConnectHandler(String HOST, int PORT)
			throws UnknownHostException, IOException {
		sock = new Socket(HOST, PORT);
		input = sock.getInputStream();
		output = sock.getOutputStream();
				
		write(SendTeamName());
		
		byte[] tmp = read();
		
		if (tmp[0] == 0){
			Main.TEAM_NUMBER = 0;
		}
		else if (tmp[0] == 1){
			Main.TEAM_NUMBER = 1;
		}
		else{
			System.err.println("Invalid response from server");
			System.exit(1);
		}
		System.out.println("Connection Complete");
		System.out.println("당신의 팀 번호는 " + Main.TEAM_NUMBER + "입니다");
		Main.testai.setTeam(Main.TEAM_NUMBER);
		Main.testai.AllowConnection = true;
	}
	
	public byte[] getNextTurn() {
		return read();
	}
	
	private void write(byte[] data) {
		try {
			System.out.println("Sent " + data.length + " bytes to server.");
			output.write(data);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	byte [] buf = new byte[1024];
	private byte[] read() {
		byte[] result = new byte[1];
		try {
			int len = input.read(buf);
			result = Arrays.copyOfRange(buf, 0, len);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		return result;
	}

	
	public void sendOrder(byte[] data) {
		if (data.length < 2){
			if (data[0] == 255)
				System.out.println("잘못된 포맷의 데이터를 보냈습니다.");
			return;
		}
		write(Main.testai.main(data));
	}

	public void sendMsg(String s) {
		
		byte[] result = new byte[41];
		result[0] = (byte) 255;
		byte[] tempmsg = new byte[0];
		try{
			tempmsg = s.getBytes("UTF8");
			if(tempmsg.length==0) tempmsg = "  ".getBytes();
		}catch(Exception e){
			
		}
		for(int i=0;i<tempmsg.length;i++){
            if (i < result.length - 1) result[i+1] = tempmsg[i];
		}
		
		write(result);	
	}
	

    private byte[] SendTeamName()
    {
        // Convert teamNumber & teamName to byte[] format / teamNumber + teamName -> data[]
    	byte[] name = new byte[0];
        try{
        	name = Main.TEAM_NAME.getBytes("UTF8");
        } catch (Exception e) {
			// TODO: handle exception
		}
        
        byte[] data = new byte[24];
        data[0] = (byte)Main.TEAM_NUMBER;
       
        for (int i = 0; i < 23; i++)
        {
            if (i < name.length) data[i + 1] = name[i];
            else data[i + 1] = 0;
        }

        return data;

    }
}
