#!/usr/bin/env python

import socket
import sys
import AI

#network config
HOST = '127.0.0.1'
PORT = 25252

teamName = "SamplePyAI"
teamNum = 0

s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
	af, socktype, proto, canonname, sa = res
	try:
		s = socket.socket(af, socktype, proto)
		s.connect(sa)
	except socket.error, msg:
		print(msg)
		s.close()
		s = None
		continue
	break

def send(msg):
	s.send(msg)

def recv():
	return str(s.recv(1024))

def exit(msg):
	print(msg)
	sys.exit(1)


if s is None:
	exit("Connection failed")
	
send(chr(teamNum) + unicode(teamName, "utf-8").encode("utf-8"))

t = recv()
if t == chr(0):
	teamNum = 0
elif t == chr(1) :
	teamNum = 1
else:
	exit("Server error: invalid msg after 'Join'")

print("Connection success: waiting ")
ai = AI.AI()
print("Your team Number is : " + str(teamNum))
ai.setTeam(teamNum)


while (ai.turn < 180):

	data = recv()
	
	print("Current turn : " + str(ai.turn))
	if (data[0] == chr(255)):
		print("You've sent wrong data")
		continue
	else:
		# Sending slime direction message using AI.py
		a = ai.main(data)
		print("Send " + str(len(a)) + "bytes")
		send(a)

	
	# Sending Team message
	if (ord(data[1]) == 10):
		send(ai.sendmsg("it's time to show my skill!"))