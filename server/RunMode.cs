﻿
namespace HAJE.SlimeAI
{
    public enum RunMode
    {
        NetworkPlay, // 네트워크 사용
        Replay, //리플레이 기록 사용;
        Demo, //내장AI 사용;
    }
}
