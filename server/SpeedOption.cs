﻿
namespace HAJE.SlimeAI
{
    public enum SpeedOption
    {
        ManuallyControl, // 수동진행 모드: 스페이스 바를 눌러서 한 턴을 진행
        NormalSpeed, // 보통진행 모드: 1초에 한 턴 진행
        FastSpeed // 빠른진행 모드: 1초에 열 턴 진행
    }
}