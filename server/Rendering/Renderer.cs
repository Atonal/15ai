﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;

namespace HAJE.SlimeAI.Rendering
{
    public enum DrawScaleAnchor
    {
        M,
        LT,
        LB,
        RT,
        RB,
    }
    public class Renderer : IDisposable
    {
        VertexType.VertexScreenPositionColorTexture[] vertices = new VertexType.VertexScreenPositionColorTexture[4];
        VertexBuffer vbo;
        ElementBuffer ebo;

        public Renderer()
        {
            vbo = VertexBuffer.CreateDynamic(vertices, ShaderProvider.ScreenSpaceTexture);
            int[] indices = new int[6];
            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 2;
            indices[4] = 1;
            indices[5] = 3;
            ebo = ElementBuffer.Create(indices);
        }

        public void Dispose()
        {
            if (vbo != null) vbo.Dispose(); vbo = null;
            if (ebo != null) ebo.Dispose(); ebo = null;
        }

        public void DrawTexture(Texture texture, Rectangle area, Color4 color)
        {
            vertices[0].TextureUV = new Vector2(0, 0);
            vertices[0].Position = new Vector2(area.Left, area.Bottom);
            vertices[0].Color = color;

            vertices[1].TextureUV = new Vector2(1, 0);
            vertices[1].Position = new Vector2(area.Right, area.Bottom);
            vertices[1].Color = color;

            vertices[2].TextureUV = new Vector2(0, 1);
            vertices[2].Position = new Vector2(area.Left, area.Top);
            vertices[2].Color = color;

            vertices[3].TextureUV = new Vector2(1, 1);
            vertices[3].Position = new Vector2(area.Right, area.Top);
            vertices[3].Color = color;

            Matrix4 identity = Matrix4.Identity;
            ShaderProvider.ScreenSpaceTexture.Bind(ref identity, GameSystem.Viewport, texture);
            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public void DrawTexture(Texture texture, Rectangle area, RectangleF rectUV, Color4 color)
        {
            vertices[0].TextureUV = new Vector2(rectUV.X, rectUV.Y );
            vertices[0].Position = new Vector2(area.Left, area.Bottom);
            vertices[0].Color = color;

            vertices[1].TextureUV = new Vector2(rectUV.X + rectUV.Width, rectUV.Y);
            vertices[1].Position = new Vector2(area.Right, area.Bottom);
            vertices[1].Color = color;

            vertices[2].TextureUV = new Vector2(rectUV.X, rectUV.Y + rectUV.Height);
            vertices[2].Position = new Vector2(area.Left, area.Top);
            vertices[2].Color = color;

            vertices[3].TextureUV = new Vector2(rectUV.X + rectUV.Width, rectUV.Y + rectUV.Height);
            vertices[3].Position = new Vector2(area.Right, area.Top);
            vertices[3].Color = color;

            Matrix4 transformMat = Matrix4.Identity;
            Matrix4 transMat = Matrix4.Identity;
            Matrix4 scaleMat = Matrix4.Identity;
            GLHelper.GetTrasformationMatrix(transMat, scaleMat, Matrix4.Identity, out transformMat);

            ShaderProvider.ScreenSpaceTexture.Bind(ref transformMat, new Vector3(GameSystem.Viewport.X, GameSystem.Viewport.Y, -1.0f), texture);

            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public void DrawTexture(Texture texture, RectangleF area, Color4 color, float scale, DrawScaleAnchor anchor)
        {
            float newX, newY, newWidth, newHeight;
            newWidth = area.Width * scale;
            newHeight = area.Height * scale;
            switch(anchor)
            {
                case DrawScaleAnchor.LB:
                    newX = area.Left;
                    newY = area.Bottom + newHeight;
                    break;
                case DrawScaleAnchor.LT:
                    newX = area.Left;
                    newY = area.Top;
                    break;
                case DrawScaleAnchor.RB:
                    newX = area.Right - newWidth;
                    newY = area.Bottom + newHeight;
                    break;
                case DrawScaleAnchor.RT:
                    newX = area.Right - newWidth;
                    newY = area.Top;
                    break;
                case DrawScaleAnchor.M:
                default:
                    newX = area.Left + (area.Width - newWidth) / 2;
                    newY = area.Top + (area.Height - newHeight) / 2;
                    break;
            }

            vertices[0].TextureUV = new Vector2(0, 0);
            vertices[0].Position = new Vector2(newX, newY + newHeight);
            vertices[0].Color = color;

            vertices[1].TextureUV = new Vector2(1, 0);
            vertices[1].Position = new Vector2(newX + newWidth, newY + newHeight);
            vertices[1].Color = color;

            vertices[2].TextureUV = new Vector2(0, 1);
            vertices[2].Position = new Vector2(newX, newY);
            vertices[2].Color = color;

            vertices[3].TextureUV = new Vector2(1, 1);
            vertices[3].Position = new Vector2(newX + newWidth, newY);
            vertices[3].Color = color;

            Matrix4 transformMat = Matrix4.Identity;
            Matrix4 transMat = Matrix4.Identity;
            Matrix4 scaleMat = Matrix4.Identity;
            GLHelper.GetTrasformationMatrix(transMat, scaleMat, Matrix4.Identity, out transformMat);

            ShaderProvider.ScreenSpaceTexture.Bind(ref transformMat, new Vector3(GameSystem.Viewport.X, GameSystem.Viewport.Y, 0.0f), texture);
            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public void DrawTexture(Texture texture, RectangleF area, Color4 color, float scale, Vector3 angle, DrawScaleAnchor anchor)
        {
            float newX, newY, newWidth, newHeight;

            newWidth = area.Width * scale;
            newHeight = area.Height * scale;

            switch (anchor)
            {
                case DrawScaleAnchor.LB:
                    newX = area.Left;
                    newY = area.Bottom + newHeight;
                    break;
                case DrawScaleAnchor.LT:
                    newX = area.Left;
                    newY = area.Top;
                    break;
                case DrawScaleAnchor.RB:
                    newX = area.Right - newWidth;
                    newY = area.Bottom + newHeight;
                    break;
                case DrawScaleAnchor.RT:
                    newX = area.Right - newWidth;
                    newY = area.Top;
                    break;
                case DrawScaleAnchor.M:
                default:
                    newX = area.Left + (area.Width - newWidth) / 2;
                    newY = area.Top + (area.Height - newHeight) / 2;
                    break;
            }

            vertices[0].TextureUV = new Vector2(0, 0);
            vertices[0].Position = new Vector2(newX, newY + newHeight);
            vertices[0].Color = color;

            vertices[1].TextureUV = new Vector2(1, 0);
            vertices[1].Position = new Vector2(newX + newWidth, newY + newHeight);
            vertices[1].Color = color;

            vertices[2].TextureUV = new Vector2(0, 1);
            vertices[2].Position = new Vector2(newX, newY);
            vertices[2].Color = color;

            vertices[3].TextureUV = new Vector2(1, 1);
            vertices[3].Position = new Vector2(newX + newWidth, newY);
            vertices[3].Color = color;

            Matrix4 transformMat = Matrix4.Identity;
            Matrix4 transMat = Matrix4.Identity;
            Matrix4 scaleMat = Matrix4.Identity;
            Matrix4 rotateMat, transOppsMat;

            Vector3 axisVec = new Vector3(-(newX + (newWidth / 2)), -(newY + (newHeight / 2)), 0.0f);
            Vector3 axisOppositeVec = new Vector3(newX + (newWidth / 2), newY + (newHeight / 2), 0.0f);

            GLHelper.GetTranslationMatrix(axisVec, out transMat);
            GLHelper.GetTranslationMatrix(axisOppositeVec, out transOppsMat);
            GLHelper.GetRotationMatrix(angle, out rotateMat);
            GLHelper.GetTrasformationMatrix(transMat, scaleMat, rotateMat, out transformMat);

            Matrix4.Mult(ref transformMat, ref transOppsMat, out transformMat);

            ShaderProvider.ScreenSpaceTexture.Bind(ref transformMat, new Vector3(GameSystem.Viewport.X, GameSystem.Viewport.Y, 0.0f), texture);
            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }
    }
}
