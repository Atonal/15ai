﻿using HAJE.SlimeAI.Rendering.VertexType;
using HAJE.SlimeAI.Rendering;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace HAJE.SlimeAI.Rendering.Shader
{
    public class ScreenSpaceTextureShader : ShaderBase
    {
        const string vertexSource = @"
                #version 150 core
                
                uniform mat4 trans;
                uniform vec3 viewPort;

                in vec2 positionIn;
                in vec4 colorIn;
                in vec2 textureIn;
                
                out vec4 color;
                out vec2 texCoord;
                
                void main()
                {
                    color = colorIn;
                    texCoord = textureIn;
                    vec4 transformed = trans * vec4(positionIn, 0, 1);
                    transformed.x = transformed.x / viewPort.x * 2 - 1.0f;
                    transformed.y = transformed.y / viewPort.y * 2 - 1.0f;
                    gl_Position = transformed;
                }
            ";

        const string fragmentSource = @"
                #version 150 core

                uniform sampler2D tex;

                in vec4 color;
                in vec2 texCoord;

                out vec4 colorOut;

                void main()
                {
                    colorOut = texture(tex, texCoord) * color;
                }
            ";

        public ScreenSpaceTextureShader()
            : base(vertexSource, fragmentSource)
        {
        }

        protected override void GetUniformLocation()
        {
            transPtr = GL.GetUniformLocation(ShaderProgram, "trans");
            viewPortPtr = GL.GetUniformLocation(ShaderProgram, "viewPort");
        }

        public void Bind(ref Matrix4 transform, Vector2 viewPort, Texture texture)
        {
            base.Bind();

            Vector3 tempVec = new Vector3(viewPort.X, viewPort.Y, 0.0f);

            GL.UniformMatrix4(transPtr, false, ref transform);
            GL.Uniform3(viewPortPtr, ref tempVec);
            GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
        }

        public void Bind(ref Matrix4 transform, Vector3 viewPort, Texture texture)
        {
            base.Bind();
            GL.UniformMatrix4(transPtr, false, ref transform);
            GL.Uniform3(viewPortPtr, ref viewPort);
            GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
        }

        int transPtr;
        int viewPortPtr;
    }
}
