﻿using HAJE.SlimeAI.Rendering.Shader;

namespace HAJE.SlimeAI.Rendering
{
    public static class ShaderProvider
    {
        public static void UnloadShader()
        {
            if (screenSpaceTexture != null) screenSpaceTexture.Dispose(); screenSpaceTexture = null;
        }

        static ScreenSpaceTextureShader screenSpaceTexture;
        public static ScreenSpaceTextureShader ScreenSpaceTexture
        {
            get
            {
                if (screenSpaceTexture == null)
                {
                    screenSpaceTexture = new ScreenSpaceTextureShader();
                }
                return screenSpaceTexture;
            }
        }
    }
}
