﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;
using System.Drawing;

namespace HAJE.SlimeAI.Rendering
{
    public static class GLHelper
    {
        [Conditional("DEBUG")]
        public static void CheckGLError()
        {
            var error = GL.GetError();
            Debug.Assert(error == ErrorCode.NoError);
        }

        public static void DrawTriangles(int count)
        {
            GL.DrawElements(PrimitiveType.Triangles, count, DrawElementsType.UnsignedInt, 0);
        }

        public static float DegreeToRadian(float angle)
        {
            return OpenTK.MathHelper.DegreesToRadians(angle);
        }
        
        public static void GetTrasformationMatrix(Matrix4 trans, Matrix4 scale, Matrix4 rotate, out Matrix4 result)
        {
            Matrix4 temp = Matrix4.Identity;

            Matrix4.Mult(ref trans, ref scale, out temp);
            Matrix4.Mult(ref temp, ref rotate, out result);
        }

        public static void GetTranslationMatrix(Vector3 vec, out Matrix4 mat)
        {
            Matrix4.CreateTranslation(ref vec, out mat);
        }

        public static void GetRotationMatrixWithAxis(Vector3 axis, Vector3 angle, out Matrix4 mat)
        {
            Matrix4 temp;

            Matrix4 rotateX = Matrix4.CreateFromAxisAngle(axis, DegreeToRadian(angle.X));
            Matrix4 rotateY = Matrix4.CreateFromAxisAngle(axis, DegreeToRadian(angle.Y));
            Matrix4 rotateZ = Matrix4.CreateFromAxisAngle(axis, DegreeToRadian(angle.Z));

            Matrix4.Mult(ref rotateX, ref rotateY, out temp);
            Matrix4.Mult(ref temp, ref rotateZ, out mat);
        }

        public static void GetRotationMatrix(Vector3 vec, out Matrix4 mat)
        {
            Matrix4 temp;

            Matrix4 rotateX = Matrix4.CreateRotationX(DegreeToRadian(vec.X));
            Matrix4 rotateY = Matrix4.CreateRotationY(DegreeToRadian(vec.Y));
            Matrix4 rotateZ = Matrix4.CreateRotationZ(DegreeToRadian(vec.Z));

            Matrix4.Mult(ref rotateX, ref rotateY, out temp);
            Matrix4.Mult(ref temp, ref rotateZ, out mat);
        }
    }
}
