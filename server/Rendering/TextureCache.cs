﻿using System;
using System.Collections.Generic;

namespace HAJE.SlimeAI.Rendering
{
    public class TextureCache : IDisposable
    {
        public Texture GetTexture(string path)
        {
            Texture texture;
            if (loadedTextures.TryGetValue(path, out texture))
            {
                return texture;
            }

            texture = Texture.CreateFromFile(path);
            loadedTextures.Add(path, texture);
            return texture;
        }

        public void AddTexture(Texture texture)
        {
            addedTextures.Add(texture);
        }

        public void RemoveTexture(Texture texture)
        {
            addedTextures.Remove(texture);
        }

        public void Dispose()
        {
            foreach (Texture texture in loadedTextures.Values)
            {
                texture.Dispose();
            }
            loadedTextures.Clear();
            foreach (Texture texture in addedTextures)
            {
                texture.Dispose();
            }
            addedTextures.Clear();
        }

        Dictionary<string, Texture> loadedTextures = new Dictionary<string, Texture>();
        List<Texture> addedTextures = new List<Texture>();
    }
}
