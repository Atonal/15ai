﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NAudio;
using NAudio.Mixer;
using NAudio.Vorbis;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;


namespace HAJE.SlimeAI.Sound
{
    public enum SoundType
    {
        None = -1,
        BackGround = 0,
        SlimeRespawn,
        TakeGround
    }

    public class SoundManager
    {
        VorbisWaveReader backgroundReader;
        VorbisWaveReader slimeRespawnReader;
        VorbisWaveReader slimeTakeGroundReader;

        ISampleProvider bgmProvider;

        public SoundManager()
        {
        }

        public void Play(SoundType type)
        {
            switch (type)
            {
                case SoundType.BackGround:
                    backgroundReader = new VorbisWaveReader("Resource/Sounds/Background.ogg");
                    bgmProvider = backgroundReader.ToSampleProvider();
                    Player.PlaySound(bgmProvider);
                    break;
                case SoundType.SlimeRespawn:
                    slimeRespawnReader = new VorbisWaveReader("Resource/Sounds/Respawn.ogg");
                    Player.PlaySound(slimeRespawnReader.ToSampleProvider());
                    break;
                case SoundType.TakeGround:
                    slimeTakeGroundReader = new VorbisWaveReader("Resource/Sounds/TakeGround.ogg");
                    Player.PlaySound(slimeTakeGroundReader.ToSampleProvider());
                    break;
                default:
                    break;
            }
        }

        public void Stop(SoundType type)
        {
            switch(type)
            {
                case SoundType.BackGround:
                    if(bgmProvider != null)
                    {
                        Player.StopSound(bgmProvider);
                    }
                    break;
                case SoundType.SlimeRespawn:
                    Player.StopSound(slimeRespawnReader);
                    break;
                case SoundType.TakeGround:
                    Player.StopSound(slimeTakeGroundReader);
                    break;
                default:
                    break;
            }
        }

        public void Pause(SoundType type)
        {

        }

        private readonly AudioPlayer Player = new AudioPlayer();
    }

    public class AudioPlayer
    {
        IWavePlayer waveOutDevice;
        MixingSampleProvider sampleProvider;

        public AudioPlayer()
        {
            waveOutDevice = new WaveOutEvent();
            sampleProvider = new MixingSampleProvider(WaveFormat.CreateIeeeFloatWaveFormat(44100, 2));
            sampleProvider.ReadFully = true;

            waveOutDevice.Init(sampleProvider);
            waveOutDevice.Play();
            waveOutDevice.Volume = 0.5f;
        }

        public void PlaySound(ISampleProvider provider)
        {
            AddMixerInput(provider);
        }

        public void StopSound(ISampleProvider provider)
        {
            sampleProvider.RemoveMixerInput(provider);
        }

        public void Dispose()
        {
            waveOutDevice.Dispose();
        }

        private void AddMixerInput(ISampleProvider input)
        {
            sampleProvider.AddMixerInput(ConvertToRightChannelCount(input));
        }

        private ISampleProvider ConvertToRightChannelCount(ISampleProvider input)
        {
            if (input.WaveFormat.Channels == sampleProvider.WaveFormat.Channels)
            {
                return input;
            }
            if (input.WaveFormat.Channels == 1 && sampleProvider.WaveFormat.Channels == 2)
            {
                return new MonoToStereoSampleProvider(input);
            }

            throw new NotImplementedException("Not yet implemented this channel count conversion");
        }
    }
}
