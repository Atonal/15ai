﻿using HAJE.SlimeAI.Rendering;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.SlimeAI
{
    public class SpriteFont
    {
        public SpriteFont(string imagePath)
        {
            if (!File.Exists(imagePath))
                throw new FileNotFoundException("폰트 이미지를 찾을 수 없습니다.\npath=" + imagePath);
            texture = GameSystem.TextureCache.GetTexture(imagePath);
            //using (Bitmap bitmap = new Bitmap(imagePath))
            //{
            //    ImageSize = new SizeF(bitmap.Width / 10, bitmap.Height);
            //    SpriteImage = new Bitmap[]{
            //        bitmap.Clone(new RectangleF(new PointF(0, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(2 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(3 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(4 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(5 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(6 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(7 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(8 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //        bitmap.Clone(new RectangleF(new PointF(9 * ImageSize.Width, 0), ImageSize), System.Drawing.Imaging.PixelFormat.DontCare),
            //    };
            //}    

        }

        public Texture makeTexture(int text, int size, Bitmap bitmap)
        {
            Texture texture = new Texture(0, new Vector2(0, 0));
            return texture;
        }

        public SizeF MeasureString(int text, int size)
        {
            return new SizeF(0, 0);
        }

        public SizeF MeasureString(int text, int size, int limit)
        {
            return new SizeF(0, 0);
        }

        Texture texture;
        Bitmap[] SpriteImage;
        SizeF ImageSize;
    }
}
