﻿using HAJE.SlimeAI.Logic;
using HAJE.SlimeAI.Scene;
using HAJE.SlimeAI.Network;
using HAJE.SlimeAI.Sound;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Timers;

namespace HAJE.SlimeAI
{
    public class MainForm : GameWindow
    {
        public const int ScreenWidth = 1024;
        public const int ScreenHeight = 768;
#if CONT
        const bool cont = true;
#else
        const bool cont = false;
#endif
#if DEV
        const bool dev = true;
#else
        const bool dev = false;
#endif
#if DEBUG
        const bool debug = true;
#else
        const bool debug = false;
#endif
        const double NormalInterval = 1000 * Logic.Simulation.TurnLength;
        const double ShortInterval = 100 * Logic.Simulation.TurnLength;

        public MainForm(RunMode mode)
            : base(ScreenWidth, ScreenHeight,
                GraphicsMode.Default, "Slime AI", GameWindowFlags.FixedWindow,
                DisplayDevice.Default, 3, 2,
                GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug)
        {
            this.mode = mode;
            X = 0;
            Y = 0;
        }

        protected override void OnLoad(System.EventArgs e)
        {
            scene = new GameScene(base.Width, base.Height);
            simulation = new Logic.Simulation();
            string[] teamNames = { "[First Team Name Here]", "[Second Team Name Here]" };
            timer.Interval = NormalInterval;
            timer.Stop();

            if (cont)
            {
                mode = RunMode.NetworkPlay;
                OnLoadNetworkPlay();
            }
            if (dev)
            {
                if (mode == RunMode.Replay)
                {
                    OnLoadReplay(teamNames);
                }
                else
                {
                    mode = RunMode.NetworkPlay;
                    OnLoadNetworkPlay();
                }
            }
            if (debug)
            {
                if (mode == RunMode.Replay)
                {
                    OnLoadReplay(teamNames);
                }
                else if (mode == RunMode.Demo)
                {
                    OnLoadDemo(teamNames);
                }
                else
                {
                    mode = RunMode.NetworkPlay;
                    OnLoadNetworkPlay();
                }
            }
            base.OnLoad(e);
        }

        private void OnLoadNetworkPlay()
        {
            scene.Initialize();
            for (int iT = 0; iT < Team.Max; iT++)
            {
                scene.SetTeamName(iT, "Team " + (iT + 1).ToString());
                scene.SetTeamMessage(iT, "...접속 대기중...");
            }
            session.OnJoin += SessionOnJoin;
            session.OnGameReady += SessionOnGameReady;
            session.OnMessage += SessionOnMessage;
            session.Start();
            timer.Elapsed += NextTurnEvent;
        }

        private void OnLoadReplay(string[] teamNames)
        {
            replay.Load();
            //state = GameState.Running;
            scene.Initialize();
            for (int iT = 0; iT < Team.Max; iT++)
            {
                teamNames[iT] = replay.TeamNames[iT];
                scene.SetTeamName(iT, teamNames[iT]);
                scene.SetTeamMessage(iT,
                "Replay Mode\n" +
                "시간과 예산을 더 주신다면");
            }
            timer.Elapsed += NextTurnEvent;
        }

        private void OnLoadDemo(string[] teamNames)
        {
            //state = GameState.Running;
            scene.Initialize();
            for (int iT = 0; iT < Team.Max; iT++)
            {
                ai[iT] = new RandomAI(iT);
                teamNames[iT] = "Demo AI " + (iT + 1).ToString();
                scene.SetTeamName(iT, teamNames[iT]);
                scene.SetTeamMessage(iT,
                "Demo Mode\n" +
                "시간과 예산을 더 주신다면");
            }
            replay.Clear(teamNames);
            timer.Elapsed += NextTurnEvent;
        }

        void NextTurnEvent(object sender, ElapsedEventArgs e)
        {
            NextTurn();
        }

        private void NextTurn()
        {
            if (cont)
            {
                mode = RunMode.NetworkPlay;
                NextNetworkTurn();
            }
            if (dev)
            {
                if (mode == RunMode.Replay)
                {
                    NextReplayTurn();
                }
                else
                {
                    mode = RunMode.NetworkPlay;
                    NextNetworkTurn();
                }
            }
            if (debug)
            {
                if (mode == RunMode.Replay)
                {
                    NextReplayTurn();
                }
                else if (mode == RunMode.Demo)
                {
                    NextDemoTurn();
                }
                else
                {
                    mode = RunMode.NetworkPlay;
                    NextNetworkTurn();
                }
            }
        }

        void SessionOnJoin(int teamIndex, string teamName)
        {
            scene.SetTeamName(teamIndex, teamName);
            scene.SetTeamMessage(teamIndex, "다른 플레이어를 기다리는 중입니다.");
            string[] replayTeamname = replay.TeamNames;
            replayTeamname[teamIndex] = teamName;
            replay.Clear(replayTeamname);
        }

        void SessionOnGameReady()
        {
            state = GameState.Running;
            session.SendNextTurn(simulation.CurrentTurn, simulation.BoardState, simulation.SlimePosition);
            scene.SetTeamMessage(0, "니 마음에 장난쳐주지");
            scene.SetTeamMessage(1, "빛이 당신을 불태울 것입니다!");
            GameSystem.SoundManager.Play(SoundType.BackGround);
            //timer.Start();
        }

        void SessionOnMessage(int teamIndex, string message)
        {
            scene.SetTeamMessage(teamIndex, message);

        }

        KeyboardState lastKeyboardState;
        Direction[,] order = new Direction[Team.Max, Simulation.MaxSlime];
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            var keyState = OpenTK.Input.Keyboard.GetState();

            scene.Update((float)e.Time);

            if (keyState.IsKeyDown(Key.F4) &&
                (keyState.IsKeyDown(Key.AltLeft) || keyState.IsKeyDown(Key.AltRight)))
            {
                Exit();
            }

            if ((state == GameState.Waiting) &&
                (KeyPressedOnce(ref keyState, Key.Space)))
            {
                if (mode == RunMode.Replay)
                {
                    state = GameState.Running;
                }
                if (mode == RunMode.Demo)
                {
                    state = GameState.Running;
                }
            }
            if (!cont)
            {
                if (KeyPressedOnce(ref keyState, Key.Number1) ||
                    KeyPressedOnce(ref keyState, Key.Keypad1))
                {
                    if (speed != SpeedOption.ManuallyControl)
                    {
                        speed = SpeedOption.ManuallyControl;
                        timer.Stop();
                    }
                }
                if (KeyPressedOnce(ref keyState, Key.Number2) ||
                    KeyPressedOnce(ref keyState, Key.Keypad2))
                {
                    if (speed == SpeedOption.ManuallyControl)
                    {
                        speed = SpeedOption.NormalSpeed;
                        timer.Interval = NormalInterval;
                    }
                    if (speed == SpeedOption.FastSpeed)
                    {
                        speed = SpeedOption.NormalSpeed;
                        timer.Interval = NormalInterval;
                    }
                }
                if (KeyPressedOnce(ref keyState, Key.Number3) ||
                    KeyPressedOnce(ref keyState, Key.Keypad3))
                {
                    if (speed == SpeedOption.ManuallyControl)
                    {
                        speed = SpeedOption.FastSpeed;
                        timer.Interval = ShortInterval;
                    }
                    if (speed == SpeedOption.NormalSpeed)
                    {
                        speed = SpeedOption.FastSpeed;
                        timer.Interval = ShortInterval;
                    }
                }
                if (KeyPressedOnce(ref keyState, Key.Slash))
                {
                    GameSystem.AnimationFlag = !GameSystem.AnimationFlag;
                }
            }

            if ((state == GameState.Running) &&
                (speed != SpeedOption.ManuallyControl) &&
                (!timer.Enabled))
            {
                timer.Start();
            }

            if (KeyPressedOnce(ref keyState, Key.Space))
            {
                if ((state == GameState.Running) &&
                    (speed == SpeedOption.ManuallyControl))
                {
                    NextTurn();
                }
            }
            if (state == GameState.Ended)
            {
                timer.Stop();
            }
            lastKeyboardState = keyState;
        }

        private bool KeyPressedOnce(ref KeyboardState keyState, Key ss)
        {
            return lastKeyboardState.IsKeyUp(ss) && keyState.IsKeyDown(ss);
        }

        private void NextNetworkTurn()
        {
            if (simulation.CurrentTurn < Simulation.MaxTurn)
            {
                Direction[,] receivedData = session.GetTurnInput(simulation.CurrentTurn);

                for (int iT = 0; iT < Team.Max; iT++)
                {
                    //var aiResult = (iT팀에서 받은 order);
                    //var aiResult = ai[iT].Run(simulation.SlimePosition, simulation.BoardState);
                    for (int iS = 0; iS < Simulation.MaxSlime; iS++)
                        order[iT, iS] = receivedData[iT, iS];
                }
                TurnWorks();
                replay.Record(simulation.CurrentTurn - 1, order);
            }

            session.SendNextTurn(simulation.CurrentTurn, simulation.BoardState, simulation.SlimePosition);
        }

        //[Conditional("DEBUG")]
        private void NextDemoTurn()
        {
            if (simulation.CurrentTurn < Simulation.MaxTurn)
            {
                for (int iT = 0; iT < Team.Max; iT++)
                {
                    var aiResult = ai[iT].Run(simulation.SlimePosition, simulation.BoardState);
                    for (int iS = 0; iS < Simulation.MaxSlime; iS++)
                        order[iT, iS] = aiResult[iS];
                }
                TurnWorks();
                replay.Record(simulation.CurrentTurn - 1, order);
            }
        }

        //[Conditional("DEV"), Conditional("DEBUG")]
        private void NextReplayTurn()
        {
            if (simulation.CurrentTurn < Simulation.MaxTurn)
            {
                for (int iT = 0; iT < Team.Max; iT++)
                {
                    order = replay.GetTurnInput(simulation.CurrentTurn);
                }
                TurnWorks();
            }
        }

        private void TurnWorks()
        {
            int restTurn = Simulation.MaxTurn - simulation.CurrentTurn-1;
            simulation.NextTurn(order);
            scene.SetTime(restTurn);
            scene.SetGameState(simulation.SlimePosition, simulation.BoardState, simulation.FlipState, (float)timer.Interval / 1000f);
            SceneScoreUpdate();
            if (0 == restTurn)
            {
                GameOver();
            }
            else if ((10 >= restTurn) && (0 == restTurn % 2))
            {
                scene.SetCountdown(restTurn / 2);
            }
        }

        private void GameOver()
        {
            scene.SetGameover();
            if (mode != RunMode.Replay)
            {
                replay.Save();
            }

            timer.Interval = 3000f;
            timer.Elapsed += SetGameResultEvent;
        }

        void SetGameResultEvent(object sender, ElapsedEventArgs e)
        {
            state = GameState.Ended;
            timer.Interval = NormalInterval;
            scene.SetGameResult(simulation.CurrentWinningTeam, simulation.Score, (float)timer.Interval / 1000f);
        }

        private void SceneScoreUpdate()
        {
            for (int iT = 0; iT < Team.Max; iT++)
            {
                scene.SetTeamScore(iT, simulation.Score[iT]);
                scene.SetTeamAccumulateScore(iT, simulation.AccScore[iT]);
                scene.SetTeamSlimeDeath(iT, simulation.TotalDeadSlimes[iT]);
                scene.SetCurrentWinningTeam(simulation.CurrentWinningTeam);
            }
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.ClearColor(Color4.Black);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            scene.Render(GameSystem.Renderer);
            SwapBuffers();
        }

        enum GameState
        {
            Waiting,
            Running,
            Ended
        }

        Logic.Simulation simulation;
        GameScene scene;
        RunMode mode;
        GameState state = GameState.Waiting;
        SpeedOption speed = SpeedOption.NormalSpeed;
        Replay replay = new Replay();
        Session session = new Session(Team.Max);
        Timer timer = new Timer();
        RandomAI[] ai = new RandomAI[Team.Max];
    }
}
