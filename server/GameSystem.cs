﻿using HAJE.SlimeAI.Rendering;
using HAJE.SlimeAI.Sound;
using OpenTK;
using System;
using System.Diagnostics;
using System.IO;

namespace HAJE.SlimeAI
{
    public static class GameSystem
    {
        public static Vector2 Viewport
        {
            get
            {
                return new Vector2(MainForm.Width, MainForm.Height);
            }
        }

        public static MainForm MainForm
        {
            get;
            private set;
        }

        public static TextureCache TextureCache
        {
            get;
            private set;
        }

        public static Renderer Renderer
        {
            get;
            private set;
        }

        public static FontManager FontManager
        {
            get;
            private set;
        }

        public static SoundManager SoundManager
        {
            get;
            private set;
        }


        public static bool AnimationFlag
        {
            get;
            set;
        }

        public static void Initialize(RunMode mode)
        {
            InitializeCurrentDirectory();
            MainForm = new MainForm(mode);
            TextureCache = new TextureCache();
            Renderer = new Renderer();
            FontManager = new SlimeAI.FontManager();
            SoundManager = new SoundManager();
            AnimationFlag = true;
            MainForm.Run();
        }

        static void InitializeCurrentDirectory()
        {
            // Resource 폴더를 일일이 복사하지 않더라도 실행 경로를 수정. 테스트 가능하도록
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                if (Debugger.IsAttached || Directory.Exists(Environment.CurrentDirectory + "\\..\\..\\Resource"))
                {
                    Environment.CurrentDirectory = "../../";
                }
            }
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                throw new FileNotFoundException("리소스 파일의 경로를 찾지 못했습니다.\n현재 경로: " + Environment.CurrentDirectory + "\n");
            }
        }

        public static void Exit()
        {
            if (MainForm != null)
            {
                MainForm.Exit();
                MainForm = null;
            }
        }

        public static void Unload()
        {
            MainForm.Dispose();
            MainForm = null;

            TextureCache.Dispose();
            TextureCache = null;

            Renderer.Dispose();
            Renderer = null;
        }
    }
}
