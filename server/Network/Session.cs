﻿using HAJE.SlimeAI.Logic;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Drawing;

namespace HAJE.SlimeAI.Network
{
    public class Session
    {
        public int Port = 25252;

        public class StateObject
        {
            public const int BUFFER_SIZE = 128;
            public byte[] buffer = new byte[BUFFER_SIZE];
            public Socket workSocket = null;
            public int teamNumber = -1;
            public int socketNumer = -1;
        }
        
        public Session(int maxTeam)
        {
            remote = new Socket[maxTeam];
            state = new StateObject[maxTeam];
            for (int i = 0; i < 200; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    SavedInput[i, j] = Direction.Default;
                }
            }
        }

        public void Start()
        {
            listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(new IPEndPoint(IPAddress.Any, Port));
            listener.Listen(5);
            listener.BeginAccept(AcceptHandler, -1);
            listener.BeginAccept(AcceptHandler, 1);
        }

        // 소켓 연결이 성립되면 호출.
        void AcceptHandler(IAsyncResult ar)
        {
            int i = (int)ar.AsyncState;
            if (i == -1) i = 0;

            remote[i] = listener.EndAccept(ar);

            state[i] = new StateObject();
            state[i].workSocket = remote[i];
            state[i].socketNumer = i;

            remote[i].BeginReceive(state[i].buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(initReceiveHandler), state[i]);
            if ((int)ar.AsyncState == -1) listener.BeginAccept(AcceptHandler, 1);
        }
        
        // AI의 접속 요청을 받으면 호출
        void initReceiveHandler(IAsyncResult ar)
        {
            StateObject so = (StateObject)ar.AsyncState;
            Socket cursoc = so.workSocket;
            int read = -1;

            try
            {
                read = cursoc.EndReceive(ar);
            }
            catch
            {
                SessionDisconnected(so);
                return;
            }

            if (read > 0) JoinHandler(so);
            else
            {
                ReceiveError(so, 0);
                return;
            }
        }
        
        // 게임 구동 이후 AI에서 보낸 데이터를 받으면 호출.
        void ReceiveHandler(IAsyncResult ar)
        {
            StateObject so = (StateObject)ar.AsyncState;
            Socket cursoc = so.workSocket;
            int read = -1;

            try
            {
                read = cursoc.EndReceive(ar);
            }
            catch
            {
                SessionDisconnected(so);
                return;
            }


            if (read > 0)
            {
                if (so.buffer[0] == 255)
                {
                    byte[] mes = new byte[40];
                    for (int i = 0; i < 40; i++)
                    {
                        mes[i] = so.buffer[i + 1];
                    }
                    String message = Encoding.UTF8.GetString(mes).Trim('\0');
                    OnMessage(so.teamNumber, message);
                }
                else if (read == 7)
                {
                    int Turn = so.buffer[0] * 256 + so.buffer[1];
                    int i = 0;
                    if (so.teamNumber == 1) i += 5;

                    if (Turn <= Simulation.MaxTurn)
                    {
                        for (int j = 0; j < Simulation.MaxSlime; j++)
                        {
                            switch (so.buffer[2 + j])
                            {
                                case 0:
                                    SavedInput[Turn, i + j] = Direction.Right;
                                    break;
                                case 1:
                                    SavedInput[Turn, i + j] = Direction.Up;
                                    break;
                                case 2:
                                    SavedInput[Turn, i + j] = Direction.Left;
                                    break;
                                case 3:
                                    SavedInput[Turn, i + j] = Direction.Down;
                                    break;
                                default:
                                    SavedInput[Turn, i + j] = Direction.Default;
                                    break;
                            }
                        }

                    }
                }
                else ReceiveError(so, 1);
            }

            so.buffer = new byte[StateObject.BUFFER_SIZE];
            try
            {
                cursoc.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReceiveHandler), so);
            }
            catch
            {
                SessionDisconnected(so);
                return;
            }
        }

        // AI에게 데이터를 보내는 함수
        void Send(StateObject so, byte[] data)
        {
            try
            {
                so.workSocket.BeginSend(data, 0, data.Length, 0, new AsyncCallback(SendHandler), so);
            }
            catch
            {
                SessionDisconnected(so);
                return;
            }
        }

        // Send에서 데이터를 보낼 때 호출
        void SendHandler(IAsyncResult ar)
        {
            StateObject so = (StateObject)ar.AsyncState;

            try
            {
                int bytesSent = so.workSocket.EndSend(ar);
            }
            catch (Exception e)
            {
                SessionDisconnected(so);
                return;
            }
        }
        
        // 접속 요청 데이터를 처리하기 위한 함수
        void JoinHandler(StateObject so)
        {           
            String teamName = Encoding.UTF8.GetString(so.buffer).Substring(1).Trim('\0');
            Socket cursoc = so.workSocket;
            int team = -1;

            if (so.buffer[0] == 1) team = 1;
            else if (so.buffer[0] == 0) team = 0;
            else
            {
                ReceiveError(so, 0);
                return;
            }

            if (!isUsed[team]) OnJoin(team, teamName);
            else if (!isUsed[1-team])
            {
                team = 1 - team;
                OnJoin(team, teamName);
            }
            else
            {
                ReceiveError(so, 0);
                return;
            }

            isUsed[team] = true;
            so.teamNumber = team;

            byte[] a = new byte[1];
            a[0] = (byte)team;
            Send(so, a);

            so.buffer = new byte[StateObject.BUFFER_SIZE];
            cursoc.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReceiveHandler), so);
            
            if (isUsed[0] && isUsed[1]) OnGameReady();
        }

        // 규격에 맞지 않는 정보를 받았을 때 호출
        void ReceiveError(StateObject so, int i)
        {
            // i : 0 = initReceive, 1 = Other
            if (i == 0)
            {
                try
                {
                    so.workSocket.Shutdown(SocketShutdown.Both);
                    so.workSocket.Close();
                }
                catch
                {
                }
                so.buffer = new byte[StateObject.BUFFER_SIZE];
                listener.BeginAccept(AcceptHandler, so.socketNumer);
                //so.workSocket.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(initReceiveHandler), so);
            }

            byte[] a = new byte[1];
            a[0] = (byte)255;
            Send(so, a);
        }

        // 소켓 연결이 끊어졌을 때 호출
        void SessionDisconnected(StateObject so)
        {
            try
            {
                so.workSocket.Shutdown(SocketShutdown.Both);
                so.workSocket.Close();
                OnError(so.teamNumber);
                isUsed[so.teamNumber] = false;
                listener.BeginAccept(AcceptHandler, so.socketNumer);
            }
            catch { }
        }
        

        // 참가자가 Join 신호를 보내면 호출 
        // 참가자가 보낸 teamIndex를 사용하는 것이 아니라, 실제 배정된 teamIndex를 사용해야 한다.
        public event TeamStringHandler OnJoin = delegate { };

        // 플레이어가 Message 신호를 보내면 호출
        public event TeamStringHandler OnMessage = delegate { };

        // 두 참가자 모두 게임에 참여 완료되어 새 게임을 시작 할 수 있을 때 호출
        public event Action OnGameReady = delegate { };

        // 네트워크 에러가 생겼다거나, 게임이 끝나기 전에 참가자의 접속이 끊겼다거나 해서
        // 게임을 계속 진행 할 수 없는 경우 호출
        public event ErrorHandler OnError = delegate { };

        public void SendNextTurn(int currentTurn, int[,] BoardState, Point[][] SlimePosition)
        {
            if (finalTurnCall) return;
            if (currentTurn == Simulation.MaxTurn) finalTurnCall = true;

            byte[] byteTurn = BitConverter.GetBytes(currentTurn);
            data[0] = byteTurn[1];
            data[1] = byteTurn[0];

            for (int i = 0; i < Simulation.Width; i++)
            {
                for (int j = 0; j < Simulation.Height; j++)
                {
                    switch(BoardState[i, j]){
                        case 0:
                            data[13 * i + j + 2] = 0;
                            break;
                        case 1:
                            data[13 * i + j + 2] = 1;
                            break;
                        case -1:
                            data[13 * i + j + 2] = 255;
                            break;
                        default:
                            break;
                    }                    
                }
            }
            for (int i=0; i<2; i++){
                for (int j = 0; j < Simulation.MaxSlime; j++)
                {
                    byte[] xPos = BitConverter.GetBytes(SlimePosition[i][j].X);
                    byte[] yPos = BitConverter.GetBytes(SlimePosition[i][j].Y);

                    
                    data[i * 10 + j * 2 + Simulation.Width * Simulation.Height + 2] = xPos[0];
                    data[i * 10 + j * 2 + Simulation.Width * Simulation.Height + 3] = yPos[0];
                }
            }

            Send(state[0], data);
            Send(state[1], data);
        }

        public Direction[,] GetTurnInput(int turnIndex)
        {
            //TODO: 주어진 턴에 맞는 입력 상태를 반환한다.
            //      SendNextTurn으로부터 +3턴까지 입력 정보는 미리 예약 할 수 있다.
            //      아무런 입력 정보가 없던 경우, 이전 턴의 입력을 사용한다.

            int maxSlime = Simulation.MaxSlime;

            Direction[,] TurnInput = new Direction[2, maxSlime];


            for (int i = 0; i <maxSlime; i++)
            {
                if (SavedInput[turnIndex, i] == Direction.Default){
                    if (turnIndex == 0)
                    {
                        TurnInput[0, i] = Direction.Left;
                    }
                    else if (SavedInput[turnIndex - 1, i] == Direction.Default)
                        TurnInput[0, i] = Direction.Left;
                    else
                    {
                        TurnInput[0, i] = SavedInput[turnIndex - 1, i];
                        SavedInput[turnIndex, i] = SavedInput[turnIndex - 1, i];
                    }
                }
                else TurnInput[0, i] = SavedInput[turnIndex, i];

            }
            for (int i = 0; i < maxSlime; i++)
            {
                if (SavedInput[turnIndex, i + maxSlime] == Direction.Default)
                {
                    if (turnIndex == 0)
                    {
                        TurnInput[0, i] = Direction.Right;
                    }
                    else if (SavedInput[turnIndex - 1, i + maxSlime] == Direction.Default)
                        TurnInput[1, i] = Direction.Left;
                    else
                    {
                        TurnInput[1, i] = SavedInput[turnIndex - 1, i + maxSlime];
                        SavedInput[turnIndex, i + maxSlime] = SavedInput[turnIndex - 1, i + maxSlime];
                    }
                }
                else TurnInput[1, i] = SavedInput[turnIndex, i + maxSlime];

            }

            return TurnInput;
        }


        Socket listener;
        Socket[] remote;
        StateObject[] state;
        bool[] isUsed = {false, false};
        bool finalTurnCall = false;
        Direction[,] SavedInput = new Direction[200, 10];
        byte[] data = new byte[2 + Simulation.Width * Simulation.Height + 4 * Simulation.MaxSlime];
    }

    public delegate void TeamStringHandler(int team, string teamName);
    public delegate void ErrorHandler(int team);
}
