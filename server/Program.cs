﻿using System;

namespace HAJE.SlimeAI
{
    class Program
    {
        static void Main(String[] args)
        {
            RunMode mode = RunMode.NetworkPlay;

            if(args.Length > 0)
            {
                mode = (RunMode)Enum.Parse(typeof(RunMode), args[0]);
            }

            GameSystem.Initialize(mode);
            GameSystem.Unload();
        }
    }
}
