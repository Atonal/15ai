﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics;
using System.Text;
using System.Drawing;
using System.Drawing.Text;

namespace HAJE.SlimeAI.Scene.Component
{
    public class TeamInfomationAreaLayer : Layer
    {
        readonly Color4[] TeamColor = { new Color4(0.78f, 0.0f, 0.0f, 1.0f), new Color4(0.0f, 0.44f, 0.75f, 1.0f) };
        readonly Color4[] ScoreColor = { new Color4(1.0f, 0.0f, 0.0f, 1.0f), new Color4(0.26f, 0.68f, 1.0f, 1.0f) };
        public TeamInfomationAreaLayer(int teamIndex, Rectangle area, Font[] font)
        {
            //teamIndex에 따라 align과 color가 정해짐
            //팀 정보를 표시하는 외각 테두리
            this.teamIndex = teamIndex;

            var infoBox = new RectangleShape();
            infoBox.Area = new Rectangle(area.X, area.Y, area.Width, area.Height);
            infoBox.FillColor = new Color4(0.0f, 0.0f, 0.0f, 0.0f);
            base.AddChild(infoBox);

            #region 팀 프로필 이미지
            teamImage = new Sprite("Resource/Profile/waiting.png");
            teamImage.Height = 80;
            teamImage.Width = teamImage.Height;

            if (teamIndex == 0)
                teamImage.Left = infoBox.Area.Left + infoBox.BorderThickness + 25;
            else
                teamImage.Right = infoBox.Area.Right - infoBox.BorderThickness - 25;
            
            teamImage.Bottom = infoBox.Area.Top + infoBox.BorderThickness + 31;
            base.AddChild(teamImage);
            #endregion

            #region 팀 이름
            teamName =  new Label("AI_Team" + teamIndex, font[1]);

            teamName.VAlignment = TextVAlignment.Top;
            teamName.Height = 18;
            teamName.Width = infoBox.Area.Width / 2 - infoBox.BorderThickness;

            if (teamIndex == 0)
            {//Team Index에 따라 정렬 기준이 달라짐
                teamName.Left = teamImage.Right + 25;
                teamName.Alignment = TextAlignment.Left;
            }
            else
            {
                teamName.Right = teamImage.Left - 25;
                teamName.Alignment = TextAlignment.Right;
            }

            teamName.Top = teamImage.Area.Bottom;
            base.AddChild(teamName);
            #endregion

            #region 팀 코멘트
            teamComment = new Label("Comment " + teamIndex, font[2]);
            teamComment.Height = 30;
            teamComment.Width = teamName.Width;
            
            if (teamIndex == 0)
            {
                teamComment.Left = teamName.Left;
                teamComment.Alignment = TextAlignment.Left;
            }
            else
            {
                teamComment.Right = teamName.Right;
                teamComment.Alignment = TextAlignment.Right;
            }

            teamComment.Top = teamName.Top - 13;
            teamComment.VAlignment = TextVAlignment.Top;
            base.AddChild(teamComment);
            #endregion

            #region 팀 슬라임 사망 수
            teamSlimeDeath = new Label("팀 슬라임 사망 수"+teamIndex, font[2]); // 팀 슬라임 사망 수
            teamSlimeDeath.Height = (infoBox.Height - 2 * infoBox.BorderThickness) / 5;
            teamSlimeDeath.Width = (infoBox.Width - 2 * infoBox.BorderThickness) / 4;

            if (teamIndex == 0)
            {
                teamSlimeDeath.Right = infoBox.Right - infoBox.BorderThickness - 10;
                teamSlimeDeath.Alignment = TextAlignment.Right;
            }
            else
            {
                teamSlimeDeath.Left = infoBox.Left + infoBox.BorderThickness + 10;
                teamSlimeDeath.Alignment = TextAlignment.Left;
            }

            //teamSlimeDeath.Top = infoBox.Top + infoBox.Height - infoBox.BorderThickness - 10;
            teamSlimeDeath.Top = infoBox.Area.Bottom - 13;
            teamSlimeDeath.VAlignment = TextVAlignment.Bottom;
            teamSlimeDeath.Color = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            base.AddChild(teamSlimeDeath);
            #endregion

            #region 팀 누적 점수
            teamAccumulateScore = new Label("팀 누적 점수"+teamIndex, font[2]); // 팀 누적 점수
            teamAccumulateScore.Height = teamSlimeDeath.Height;
            teamAccumulateScore.Width = teamSlimeDeath.Width;

            if (teamIndex == 0)
            {
                teamAccumulateScore.Right = teamSlimeDeath.Right;
                teamAccumulateScore.Alignment = TextAlignment.Right;
            }
            else
            {
                teamAccumulateScore.Left = teamSlimeDeath.Left;
                teamAccumulateScore.Alignment = TextAlignment.Left;
            }

            teamAccumulateScore.Top = teamSlimeDeath.Top - 10;
            //teamAccumulateScore.Color = new Color4(0.5f, 0.5f, 0.5f, 1.0f);
            teamAccumulateScore.Color = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            teamAccumulateScore.VAlignment = TextVAlignment.Top;
            base.AddChild(teamAccumulateScore);
            #endregion

            #region 팀 점수
            teamScore = new Label("팀 점수"+teamIndex, font[0]);      // 팀 점수
            teamScore.Height = (infoBox.Height - 2 * infoBox.BorderThickness) / 2;
            teamScore.Width = teamAccumulateScore.Width;

            if (teamIndex == 0)
            {
                teamScore.Right = teamAccumulateScore.Right + 10;
                teamScore.Alignment = TextAlignment.Right;
            }
            else
            {
                teamScore.Left = teamAccumulateScore.Left - 10;
                teamScore.Alignment = TextAlignment.Left;
            }

            teamScore.Top = teamAccumulateScore.Top - 10;
            teamScore.VAlignment = TextVAlignment.Top;
            teamScore.Color = ScoreColor[teamIndex];
            base.AddChild(teamScore);
            #endregion
        }

        public override void Render(Rendering.Renderer renderer)
        {
            base.Render(renderer);
        }

        public void SetTeamName(string teamName)
        {
            this.teamName.Text = teamName;

            try
            {
                teamImage.UpdatePath("Resource/Profile/" + teamName + ".png");
            }
            catch (System.IO.FileNotFoundException)
            {
                if (this.teamIndex == Logic.Team.Red)
                    teamImage.UpdatePath("Resource/Profile/red.png");
                else
                    teamImage.UpdatePath("Resource/Profile/blue.png");
            }
        }

        public string GetTeamName()
        {
            return this.teamName.Text;
        }

        public void SetTeamMessage(string message)
        {
            this.teamComment.Text = message;
        }

        public void SetTeamScore(int score)
        {
            this.teamScore.Text = score.ToString();
        }

        public void SetTeamAccumulateScore(int accScore)
        {
            this.teamAccumulateScore.Text = accScore.ToString();
        }

        public void SetTeamSlimeDeath(int death)
        {
            this.teamSlimeDeath.Text = death.ToString();
        }

        int teamIndex;
        Sprite teamImage;     // 팀 이미지
        Label teamName;       // 팀 이름
        Label teamComment;    // 팀 메세지
        Label teamScore;      // 팀 점수
        Label teamAccumulateScore;    // 팀 누적 점수
        Label teamSlimeDeath; // 팀 슬라임 사망 수
    }
}
