﻿using HAJE.SlimeAI.Scene.Component;
using OpenTK.Graphics;
using System.Drawing;
using System.Collections;

using HAJE.SlimeAI.Sound;

namespace HAJE.SlimeAI.Scene
{
    public class BattleGroundLayer : Layer
    {
        int gridWidth = Logic.Simulation.Width;
        int gridHeight = Logic.Simulation.Height;
        int maxTeam = Logic.Team.Max;
        int maxSlime = Logic.Simulation.MaxSlime;

        //게임이 완료되었는가
        bool gameEnded = false;

        //슬라임 초기 위치 from HAJE.SlimeAI.Logic 
        //TODO : 값 정의 위치 fix 
        readonly Point[] redSlimeInitialPosition = { new Point(0, 13 - 1), new Point(0, 13 / 2), new Point(0, 0), new Point(25 / 4, 13 / 4), new Point(25 / 4, 13 - 1 - 13 / 4) };
        readonly Point[] blueSllimeInitialPosition = { new Point(25 - 1, 13 - 1), new Point(25 - 1, 13 / 2), new Point(25 - 1, 0), new Point(25 - 1 - 25 / 4, 13 / 4), new Point(25 - 1 - 25 / 4, 13 - 1 - 13 / 4) };

        public BattleGroundLayer(Rectangle area)
        {
            mainBox = new RectangleShape();
            xSize = (int)((area.Width - 8) / gridWidth);
            ySize = (int)((area.Height - 8) / gridHeight);
            xGap = area.X + (int)((area.Width - 8 - xSize * gridWidth) / 2);
            yGap = area.Y + area.Height - 8 - ySize * gridHeight;
            mainBox.Area = new Rectangle(xGap, yGap, 8 + xSize * gridWidth, 8 + ySize * gridHeight);
            mainBox.BorderThickness = 4;
            mainBox.BorderColor = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            mainBox.FillColor = new Color4(0.0f, 0.0f, 0.0f, 1.0f);
            //TODO : 이전 보드 State도 읽을 수 있도록

            base.AddChild(mainBox);

            redSlime = new Animation[] { new Animation("Resource/RedSlimeAnimation.png", 4), new Animation("Resource/RedSlimeAnimation.png", 4), new Animation("Resource/RedSlimeAnimation.png", 4), new Animation("Resource/RedSlimeAnimation.png", 4), new Animation("Resource/RedSlimeAnimation.png", 4) };
            blueSlime = new Animation[] { new Animation("Resource/BlueSlimeAnimation.png", 4), new Animation("Resource/BlueSlimeAnimation.png", 4), new Animation("Resource/BlueSlimeAnimation.png", 4), new Animation("Resource/BlueSlimeAnimation.png", 4), new Animation("Resource/BlueSlimeAnimation.png", 4) };
            board = new Tile[gridWidth, gridHeight];
            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    board[i, j] = new Tile();
                    board[i, j].Area = new Rectangle(xGap + mainBox.BorderThickness + i * xSize, yGap + mainBox.BorderThickness + j * ySize, xSize, ySize);
                    //board[i, j].Color = new Color4(0.0f, 0.0f, 0.0f, 0.0f);
                    board[i, j].State = -1;
                    base.AddChild(board[i, j]);
                }
            }

            for (int i = 0; i < maxSlime; i++)
            {
                //슬라임 초기 위치 from HAJE.SlimeAI.Logic 
                //TODO : 새 슬라임 객체 
                redSlime[i].Width = blueSlime[i].Width = xSize;
                redSlime[i].Height = blueSlime[i].Height = ySize;
                redSlime[i].PositionSet(new Point(xGap + mainBox.BorderThickness + redSlimeInitialPosition[i].X * xSize, yGap + mainBox.BorderThickness + redSlimeInitialPosition[i].Y * ySize));
                blueSlime[i].PositionSet(new Point(xGap + mainBox.BorderThickness + blueSllimeInitialPosition[i].X * xSize, yGap + mainBox.BorderThickness + blueSllimeInitialPosition[i].Y * ySize));
                board[redSlimeInitialPosition[i].X, redSlimeInitialPosition[i].Y].State = 0;
                board[blueSllimeInitialPosition[i].X, blueSllimeInitialPosition[i].Y].State = 1;
                base.AddChild(redSlime[i]);
                base.AddChild(blueSlime[i]);
            }

            effectLayer = new EffectLayer();
            base.AddChild(effectLayer);
        }

        public void SetGameState(Point[][] slimePos, int[,] boardState, int[,] flipState, float seconds)
        {
            bool soundEffect = false;
            float[] DelayTime = { seconds / 2, seconds / 2 };
            float DelayTimeIncrementOverTiles = seconds * 0.02f;
            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    if (boardState[i, j] != board[i, j].State)
                    {
                        if (GameSystem.AnimationFlag)
                        {
                            //board[i, j].State = boardState[i, j];
                            if (flipState[i, j] < 0)
                            {
                                board[i, j].SetColorChangeEffect(TileEffectState.Normal, boardState[i, j], seconds);
                                if (!soundEffect)
                                {
                                    soundEffect = true;
                                    GameSystem.SoundManager.Play(SoundType.TakeGround);
                                }
                            }
                            else
                            {
                                board[i, j].edgeInfo.SetAll(false);
                                if (flipState[i, j - 1] != flipState[i, j])
                                    board[i, j].edgeInfo[0] = true;
                                if (flipState[i, j + 1] != flipState[i, j])
                                    board[i, j].edgeInfo[1] = true;
                                if (flipState[i - 1, j] != flipState[i, j])
                                    board[i, j].edgeInfo[2] = true;
                                if (flipState[i + 1, j] != flipState[i, j])
                                    board[i, j].edgeInfo[3] = true;

                                effectLayer.AddTileFlipBorderEffect(seconds, seconds / 2f, board[i, j].Area, board[i, j].edgeInfo);
                                board[i, j].SetColorChangeEffect(TileEffectState.Flip, boardState[i, j], seconds * 0.7f, DelayTime[boardState[i, j]]);
                                effectLayer.AddTileFlipParticleEffect(board[i, j].Area, boardState[i, j], 4, DelayTime[boardState[i, j]] + seconds * 0.2f);

                                DelayTime[boardState[i, j]] += DelayTimeIncrementOverTiles;
                            }
                        }
                        else
                        {
                            board[i, j].State = boardState[i, j];
                        }


                    }
                }
            }

            //슬라임 이동 설정
            for (int i = 0; i < maxSlime; i++)
            {
                if (GameSystem.AnimationFlag)
                {
                    redSlime[i].Move(targetPosition: new PointF(xGap + mainBox.BorderThickness + slimePos[0][i].X * xSize, yGap + mainBox.BorderThickness + slimePos[0][i].Y * ySize),
                        seconds: seconds);
                    blueSlime[i].Move(targetPosition: new PointF(xGap + mainBox.BorderThickness + slimePos[1][i].X * xSize, yGap + mainBox.BorderThickness + slimePos[1][i].Y * ySize),
                        seconds: seconds);
                }
                else
                {
                    redSlime[i].Area = new Rectangle(new Point(xGap + mainBox.BorderThickness + slimePos[0][i].X * xSize, yGap + mainBox.BorderThickness + slimePos[0][i].Y * ySize), new Size(xSize, ySize));
                    blueSlime[i].Area = new Rectangle(new Point(xGap + mainBox.BorderThickness + slimePos[1][i].X * xSize, yGap + mainBox.BorderThickness + slimePos[1][i].Y * ySize), new Size(xSize, ySize));
                }
                //새 스테이트가 들어오면 그 쪽으로 슬라임들을 이동
            }

        }

        public void UpdateGameState(float elapsedTime)
        {
            for (int i = 0; i < maxSlime; i++)
            {
                redSlime[i].Update(elapsedTime);
                blueSlime[i].Update(elapsedTime);
            }

            for (int i = 0; i < gridWidth; i++)
                for (int j = 0; j < gridHeight; j++)
                    board[i, j].Update(elapsedTime);

            effectLayer.UpdateGameState(elapsedTime);
        }

        public void SetGameResult(int winningTeam, string teamName, int[] TeamScore, float seconds)
        {
            gameEnded = true;
            bool soundEffect = false;
            float DelayTime = 0f;
            float DelayTimeIncrementOverTiles = seconds * 0.04f;
            int[] Score = { TeamScore[0], TeamScore[1] };
            int flipSlowStart = 15; //타일 뒤집기가 느려지기 시작할 때 적은 쪽의 타일 수
            int flipSlowNum = 8;    //느려지는 횟수
            bool allLessTileFliped = false; //적은 쪽 타일이 모두 뒤집혔는가
            float gazeTime = seconds * 2f;  //남은 타일을 한번에 뒤집기 전 대기 시간

            GameSystem.SoundManager.Stop(SoundType.BackGround);


            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    if (GameSystem.AnimationFlag)
                    {
                        //판 전체를 default 색으로 칠하기
                        board[i, j].SetColorChangeEffect(TileEffectState.Normal, Logic.Team.Neutral, seconds, 2f);
                        if (!soundEffect)
                        {
                            soundEffect = true;
                            GameSystem.SoundManager.Play(SoundType.TakeGround);
                        }
                    }
                    else
                    {
                        board[i, j].State = Logic.Team.Neutral;
                    }

                }
            }
            //슬라임 아래로 치우기
            for (int i = 0; i < maxSlime; i++)
            {
                if (GameSystem.AnimationFlag)
                {
                    redSlime[i].Move(targetPosition: new PointF(xGap + mainBox.BorderThickness + i * xSize, yGap + mainBox.BorderThickness - 1 * ySize),
                        seconds: seconds);
                    blueSlime[i].Move(targetPosition: new PointF(Width - xGap - mainBox.BorderThickness - (i+1) * xSize, yGap + mainBox.BorderThickness - 1 * ySize),
                        seconds: seconds);
                }
                else
                {
                    redSlime[i].Area = new Rectangle(new Point(xGap + mainBox.BorderThickness + i * xSize, yGap + mainBox.BorderThickness - 1 * ySize), new Size(xSize, ySize));
                    blueSlime[i].Area = new Rectangle(new Point(Width - xGap - mainBox.BorderThickness - (i + 1) * xSize, yGap + mainBox.BorderThickness - 1 * ySize), new Size(xSize, ySize));
                }
            }

            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    if (GameSystem.AnimationFlag)
                    {
                        board[i, j].edgeInfo.SetAll(false);
                    }
                    else
                    {
                        //board[i, j].State = 0;
                    }
                }
            }


            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    if (GameSystem.AnimationFlag)
                    {
                        board[i, j].edgeInfo.SetAll(false);

                        float d = DelayTime;
                        if (Score[Logic.Team.Red] > 0)
                        {
                            Score[Logic.Team.Red]--;
                            board[i, j].EndHandler +=
                                (self) =>
                                {
                                    effectLayer.AddTileFlipBorderEffect(seconds, seconds / 2f, self.Area, self.edgeInfo);
                                    self.SetColorChangeEffect(TileEffectState.Normal, Logic.Team.Red, seconds * 0.7f, d);
                                    effectLayer.AddTileFlipParticleEffect(self.Area, Logic.Team.Red, 4, d + seconds * 0.2f);
                                };
                        }

                        if (Score[Logic.Team.Blue] > 0)
                        {
                            Score[Logic.Team.Blue]--;
                            board[gridWidth - i - 1, gridHeight - j - 1].EndHandler +=
                                (self) =>
                                {
                                    effectLayer.AddTileFlipBorderEffect(seconds, seconds / 2f, self.Area, self.edgeInfo);
                                    self.SetColorChangeEffect(TileEffectState.Normal, Logic.Team.Blue, seconds * 0.7f, d);
                                    effectLayer.AddTileFlipParticleEffect(self.Area, Logic.Team.Blue, 4, d + seconds * 0.2f);
                                };
                        }
                        if ((Score[Logic.Team.Red] < 1) || (Score[Logic.Team.Blue] < 1))
                        {
                            if (!allLessTileFliped)
                            {
                                allLessTileFliped = !allLessTileFliped;
                                DelayTime += gazeTime;
                            }
                            //상대와 비교할 때 더 많은 부분을 빠르게 뒤집는다.
                            DelayTimeIncrementOverTiles = seconds * 0.02f;
                        }
                        else if ((Score[Logic.Team.Red] < flipSlowStart) || (Score[Logic.Team.Blue] < flipSlowStart))
                        {
                            //타일 뒤집는 속도는 여기서 느려진다.
                            if (flipSlowNum > 0)
                            {
                                DelayTimeIncrementOverTiles*=1.3f;
                                flipSlowNum--;
                            }
                        }
                        DelayTime += DelayTimeIncrementOverTiles;
                    }
                    else
                    {
                        board[i, j].State = 0;
                    }
                }
            }

            effectLayer.SetGameResultScreen(mainBox.Area, winningTeam, teamName, seconds, DelayTime);
        }

        public void SetCountdown(int count)
        {
            effectLayer.SetCountdown(mainBox.Area, count);
        }

        public void SetGameover()
        {
            effectLayer.SetGameover(mainBox.Area);
        }

        public override void Render(Rendering.Renderer renderer)
        {
            base.Render(renderer);
        }

        int xSize, ySize;
        int xGap, yGap;
        RectangleShape mainBox;
        Animation[] redSlime;
        Animation[] blueSlime;
        Tile[,] board;

        EffectLayer effectLayer;
    }
}
