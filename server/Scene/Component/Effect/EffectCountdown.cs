﻿using HAJE.SlimeAI.Scene.Component;
using HAJE.SlimeAI.Rendering;
using HAJE.SlimeAI.Logic;

using OpenTK;
using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections;
using System.Collections.Generic;


namespace HAJE.SlimeAI.Scene.Component
{
    class EffectCountdown : Effect
    {
        int countdown;
        Texture countdownTexture;
        public EffectCountdown(int count, Rectangle area, float effectTime)
        {
            int side = (area.Width > area.Height) ? area.Height : area.Width;
            Area = new Rectangle(area.Left+(area.Width-side)/2,area.Top+(area.Height-side)/2,side,side);
            if (count > 5)
            {
                countdown = 5;
            }
            else if (count < 1)
            {
                countdown = 1;
            }
            else
            {
                countdown = count;
            }
            countdownTexture = null;
            delayTimer = 0f;
            effectTimer = effectTime;
            maxEffectTime = effectTimer;
        }

        public override void Render(Renderer renderer)
        {
            if (effectTimer > 0)
            {
                float rate = (effectTimer / maxEffectTime);
                if (countdown != 0)
                {
                    if (countdownTexture == null)
                    {
                        countdownTexture = GameSystem.TextureCache.GetTexture("Resource/count_" + countdown.ToString() + ".png");
                    }
                    renderer.DrawTexture(countdownTexture, Area, new Color4(1f, 1f, 1f, rate));
                }
                else
                {
                    if (countdownTexture == null)
                    {
                        countdownTexture = GameSystem.TextureCache.GetTexture("Resource/gameover.png");
                    }
                    renderer.DrawTexture(countdownTexture, Area, new Color4(1f, 1f, 1f, 1f - rate * rate));
                } 
            }
        }

        public override void Update(float dt)
        {
            base.Update(dt);
        }

    }
}
