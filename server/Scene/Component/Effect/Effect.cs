﻿using HAJE.SlimeAI.Rendering;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.SlimeAI.Scene.Component
{
    public class Effect : Node
    {
        public Effect()
        {

        }
        public virtual void Update(float dt)
        {
            if (effectTimer > 0)
            {
                if (delayTimer > 0)
                {
                    delayTimer -= dt;
                    if (delayTimer < 0)
                    {
                        //초과된 시간을 effectTimer로 넘기고 종료
                        effectTimer += delayTimer;
                        delayTimer = 0;
                    }
                }
                else
                {
                    effectTimer -= dt;
                    if (effectTimer < 0)
                        effectTimer = 0;
                }
            }
        }

        public override void Render(Renderer renderer)
        {
            base.Render(renderer);
        }

        public virtual bool IsFinished()
        {
            return !(effectTimer > 0);
        }

        protected float effectTimer;
        protected float maxEffectTime;
        protected float delayTimer;
        protected Texture texture;
    }
}
