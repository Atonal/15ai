﻿using HAJE.SlimeAI.Scene.Component;
using HAJE.SlimeAI.Rendering;

using OpenTK;
using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections;
using System.Collections.Generic;

namespace HAJE.SlimeAI.Scene.Component
{
    public class FlipParticle : Node
    {
        string texturePath;
        Texture texture;
        bool isTextureDirty;

        Vector2 position;
        Vector2 velocity;
        Vector2 accel;

        float maxLifeTime;
        float lifeTime;
        float alpha;

        float scale;
        
        public FlipParticle(string _TexturePath, Vector2 _Position, Vector2 _Velocity, Vector2 _Accel, int size, float Scale, float LifeTime)
        {
            Initialize(_TexturePath, _Position, _Velocity, _Accel, size, Scale, LifeTime);
        }

        public void Initialize(string _TexturePath, Vector2 _Position, Vector2 _Velocity, Vector2 _Accel, int size, float Scale, float LifeTime)
        {
            texturePath = _TexturePath;
            isTextureDirty = true;
            position = _Position;
            velocity = _Velocity;
            accel = _Accel;
            Area = new Rectangle((int)position.X, (int)position.Y, size, size);
            maxLifeTime = LifeTime;
            lifeTime = maxLifeTime;

            scale = Scale;

            alpha = 1f;
        }

        public void Update(float dt)
        {
            if (IsDead())
                return;
            lifeTime -= dt;
            position += velocity * dt;
            velocity += accel * dt;

            Area = new Rectangle(new Point((int)position.X, (int)position.Y), Area.Size);
            alpha = lifeTime / maxLifeTime;
        }

        public bool IsDead()
        {
            return !(lifeTime > 0);
        }

        public override void Render(Renderer renderer)
        {
            if (IsDead())
                return;
            if (isTextureDirty)
            {
                texture = GameSystem.TextureCache.GetTexture(texturePath);
                isTextureDirty = false;
            }
            renderer.DrawTexture(texture, this.Area, new Color4(1f,1f,1f,alpha), scale, DrawScaleAnchor.M);
        }
    
    }
    public class EffectFlipParticle : Effect
    {
        public EffectFlipParticle(int Team, int ParticleNum, Rectangle Area, float delay ,float effectScale)
        {
            SetDelayTime(delay);
            AddParticle(Team, ParticleNum, Area, effectScale);
        }

        public void SetDelayTime(float delayTime)
        {
            delayTimer = delayTime;
        }
        public void AddParticle(int Team, int Num, Rectangle Area,float effectScale)
        {
            this.Area = Area;
            for (int i = 0; i < Num; i++)
                AddParticle(Team);
        }

        public void AddParticle(int Team)
        {
            FlipParticle newParticle = null;
            foreach (FlipParticle p in particles)
            {
                if (p.IsDead())
                {
                    newParticle = p;
                    break;
                }
            }


            float positionWidthRandomValue = random.Next(0, Area.Width);
            float positionHeightRandomValue = random.Next(0, Area.Height);
            int velocityXFactor = (positionWidthRandomValue > Area.Width / 2) ? 1 : -1;
            int velocityYFactor = (positionHeightRandomValue > Area.Height / 2) ? 1 : -1;

            Vector2 newPosition = new Vector2(Area.X - 20 + positionWidthRandomValue, Area.Y - 20 + positionHeightRandomValue);
            Vector2 newVeloctiy = new Vector2(random.Next(20,40) * velocityXFactor,random.Next(20,40) *velocityYFactor);
            float newScale = (float)random.Next(60,120) / 100f;
            string textureName = (Team == 0) ? "Resource/redEffect.png" : "Resource/blueEffect.png";

            if (newParticle == null)
            {
                newParticle = new FlipParticle(textureName, newPosition, newVeloctiy, -newVeloctiy, 40, newScale, 1.0f);
                particles.Add(newParticle);
            }
            else
            {
                newParticle.Initialize(textureName, newPosition, newVeloctiy, -newVeloctiy, 40, newScale, 1.0f);
            }
        }

        public override void Render(Renderer renderer)
        {
            if (delayTimer > 0)
                return;

            foreach (FlipParticle p in particles)
            {
                p.Render(renderer);
                //renderer.DrawTexture(GameSystem.TextureCache.GetTexture("Resource/redEffect.png"), Area, Color4.White);
            }
        }

        public override void Update(float dt)
        {
            if (delayTimer > 0)
            {
                delayTimer -= dt;
                return;
            }
            foreach (FlipParticle p in particles)
            {
                p.Update(dt);
            }
        }

        public override bool IsFinished()
        {
            foreach (FlipParticle p in particles)
            {
                if (!p.IsDead())
                    return false;
            }
            return true;
        }


        List<FlipParticle> particles = new List<FlipParticle>();
        static Random random = new Random();
    }
}
