﻿using HAJE.SlimeAI.Scene.Component;
using HAJE.SlimeAI.Rendering;
using HAJE.SlimeAI.Logic;

using OpenTK;
using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections;
using System.Collections.Generic;


namespace HAJE.SlimeAI.Scene.Component
{
    class EffectGameover : Effect
    {
        Texture countdownTexture;
        public EffectGameover(Rectangle area, float effectTime, float delayTime = 0f)
        {
            int side = (area.Width > area.Height) ? area.Height : area.Width;
            Area = new Rectangle(area.Left+(area.Width-side)/2,area.Top+(area.Height-side)/2,side,side);
            countdownTexture = null;
            delayTimer = 0f;
            effectTimer = effectTime;
            maxEffectTime = effectTimer;
        }

        public override void Render(Renderer renderer)
        {
            if (effectTimer > 0)
            {
                float rate = 1f - (effectTimer / maxEffectTime);
                    if (countdownTexture == null)
                    {
                        countdownTexture = GameSystem.TextureCache.GetTexture("Resource/gameover.png");
                    }
                    float modifiedRate;
                    int x = Area.Left;
                    int y = Area.Top;
                    int width = Area.Width;
                    int height = Area.Height;
                    Rectangle newArea;
                    if (rate < 0.4)
                    {
                        rate = rate/0.4f;
                        modifiedRate = (float)(Math.Sin(-13 * Math.PI / 2 * (rate + 1)) * Math.Pow(2, -10 * rate)) + 1;//sin(-13 * M_PI_2 * (p + 1)) * pow(2, -10 * p) + 1
                        x = Area.Left + (int)(Area.Width / 2 * (1 - modifiedRate));
                        y = Area.Top + (int)(Area.Height / 2 * (1 - modifiedRate));
                        width = (int)(Area.Width * modifiedRate);
                        height = (int)(Area.Height * modifiedRate);
                        newArea = new Rectangle(x, y, width, height);
                    }
                    else if (rate > 0.9)
                    {
                        rate = (1 - (rate - 0.9f) / 0.1f);
                        modifiedRate = rate * rate * rate * rate;
                        x = Area.Left + (int)(Area.Width / 2 * (1 - modifiedRate));
                        y = Area.Top + (int)(Area.Height / 2 * (1 - modifiedRate));
                        width = (int)(Area.Width * modifiedRate);
                        height = (int)(Area.Height * modifiedRate);
                        newArea = new Rectangle(x, y, width, height);
                    }
                    else
                    {
                        newArea = new Rectangle(x, y, width, height);
                    }
                    renderer.DrawTexture(countdownTexture, newArea, new Color4(1f, 1f, 1f, 1f));
            }
        }

        public override void Update(float dt)
        {
            base.Update(dt);
        }

    }
}
