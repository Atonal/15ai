﻿using HAJE.SlimeAI.Scene.Component;
using HAJE.SlimeAI.Rendering;

using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections;
using System.Collections.Generic;


namespace HAJE.SlimeAI.Scene.Component
{
    public class EffectOutline : Effect
    {
        public EffectOutline(float effectTime, float delayTime, Rectangle area, BitArray edge)
        {
            //renderer.DrawTexture(GameSystem.TextureCache.GetTexture("Resource/redEffect.png"), Area, Color4.White);
            texture = GameSystem.TextureCache.GetTexture("Resource/white.png");
            InitializeInfo(effectTime, delayTime, area, edge);
        }

        public void InitializeInfo(float effectTime, float delayTime, Rectangle area, BitArray edge)
        {
            effectTimer = effectTime;
            maxEffectTime = effectTimer;
            delayTimer = delayTime;
            edgeInfo = edge;
            this.Area = area;
        }

        public override void Render(Renderer renderer)
        {
            if (effectTimer > 0)
            {
                int BorderThickness = 4;
                Color4 borderColor = new Color4(1f, 1f, 1f, delayTimer * 2);
                if (edgeInfo[0])
                {
                    Rectangle topArea = new Rectangle(Area.X, Area.Y, Area.Width, BorderThickness);
                    renderer.DrawTexture(texture, topArea, borderColor);
                }

                if (edgeInfo[1])
                {
                    Rectangle bottomArea = new Rectangle(Area.Left, Area.Bottom - BorderThickness, Area.Width, BorderThickness);
                    renderer.DrawTexture(texture, bottomArea, borderColor);
                }

                if (edgeInfo[2])
                {
                    Rectangle leftArea = new Rectangle(Area.X, Area.Y, BorderThickness, Area.Height);
                    renderer.DrawTexture(texture, leftArea, borderColor);
                }

                if (edgeInfo[3])
                {
                    Rectangle rightArea = new Rectangle(Area.Right - BorderThickness, Area.Y, BorderThickness, Area.Height);
                    renderer.DrawTexture(texture, rightArea, borderColor);
                }
            }
        }

        public override void Update(float dt)
        {
            base.Update(dt);
        }

        public override bool IsFinished()
        {
            return base.IsFinished();
        }

        BitArray edgeInfo;
        Color4 effectColor = Color4.White;
        Color4 baseColor = Color4.White;

    }
}
