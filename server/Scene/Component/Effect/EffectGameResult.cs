﻿using HAJE.SlimeAI.Scene.Component;
using HAJE.SlimeAI.Rendering;
using HAJE.SlimeAI.Logic;

using OpenTK;
using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections;
using System.Collections.Generic;


namespace HAJE.SlimeAI.Scene.Component
{
    class EffectGameResult : Effect
    {
        int winningTeam;
        string winningTeamName;
        Texture winningTeamTexture;
        Texture winningTeamProfileTexture;
        Rectangle teamProfileArea;
        public EffectGameResult(int winningTeamIdx, Rectangle area, string teamName, float effectTime, float delayTime = 0f)
        {
            Area = area;
            teamProfileArea = new Rectangle(area.X + (int)(area.Width * 0.2f), area.Y + (int)(area.Height * 0.35f), (int)(area.Width * 0.25f), (int)(area.Height * 0.4f));
            winningTeam = winningTeamIdx;
            winningTeamTexture = null;
            winningTeamProfileTexture = null;
            winningTeamName = teamName;
            delayTimer = delayTime;
            effectTimer = effectTime;
            maxEffectTime = effectTimer;
        }

        public override void Render(Renderer renderer)
        {
            if (winningTeamTexture == null)
                switch (winningTeam)
                {
                    case Team.Red:
                        winningTeamTexture = GameSystem.TextureCache.GetTexture("Resource/Result_Red.png");
                        break;
                    case Team.Blue:
                        winningTeamTexture = GameSystem.TextureCache.GetTexture("Resource/Result_Blue.png");
                        break;
                    case Team.Neutral:
                    default:
                        winningTeamTexture = GameSystem.TextureCache.GetTexture("Resource/Result_Blue.png");
                        break;
                }

            if(winningTeamProfileTexture == null)
            {
                try
                {
                    winningTeamProfileTexture = GameSystem.TextureCache.GetTexture("Resource/Profile/" + winningTeamName + ".png");
                }
                catch (FileNotFoundException)
                {
                    switch (winningTeam)
                    {
                        case Team.Red:
                            winningTeamProfileTexture = GameSystem.TextureCache.GetTexture("Resource/Profile/red.png");
                            break;
                        case Team.Blue:
                            winningTeamProfileTexture = GameSystem.TextureCache.GetTexture("Resource/Profile/blue.png");
                            break;
                        case Team.Neutral:
                        default:
                            winningTeamProfileTexture = GameSystem.TextureCache.GetTexture("Resource/White.png");
                            break;
                    }
                }
            }

            renderer.DrawTexture(winningTeamTexture, Area, new Color4(1f, 1f, 1f, 1f - (effectTimer / maxEffectTime)));
            renderer.DrawTexture(winningTeamProfileTexture, teamProfileArea, new Color4(1f, 1f, 1f, 1f - (effectTimer / maxEffectTime)));
        }

        public override void Update(float dt)
        {
            base.Update(dt);
        }

        public override bool IsFinished()
        {
            //없어지지않고 계속 남아있는 이펙트
            return false;
        }
    }
}
