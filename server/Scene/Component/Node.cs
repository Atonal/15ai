﻿using HAJE.SlimeAI.Rendering;
using OpenTK;
using System.Drawing;

namespace HAJE.SlimeAI.Scene.Component
{
    public class Node
    {
        public Node()
        {
            Scale = 1.0f;
            Angle = Vector3.Zero;
        }

        public virtual void Render(Renderer renderer)
        {

        }

        public int X
        {
            get
            {
                return area.X;
            }
            set
            {
                area.X = value;
                OnAreaChanged();
            }
        }

        public int Left
        {
            get
            {
                return X;
            }
            set
            {
                X = value;
            }
        }

        public int Right
        {
            get
            {
                return area.Right;
            }
            set
            {
                X = value - area.Width;
                OnAreaChanged();
            }
        }

        public int Y
        {
            get
            {
                return area.Y;
            }
            set
            {
                area.Y = value;
                OnAreaChanged();
            }
        }

        public int Bottom
        {
            get
            {
                return Y;
            }
            set
            {
                area.Y = value;
                OnAreaChanged();
            }
        }

        public int Top
        {
            get
            {
                return area.Top;
            }
            set
            {
                area.Y = value - area.Height;
                OnAreaChanged();
            }
        }

        public int Height
        {
            get
            {
                return area.Height;
            }
            set
            {
                area.Height = value;
                OnAreaChanged();
            }
        }

        public int Width
        {
            get
            {
                return area.Width;
            }
            set
            {
                area.Width = value;
                OnAreaChanged();
            }
        }

        Rectangle area;
        public Rectangle Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
                OnAreaChanged();
            }
        }

        public float Scale { get; private set; }
        public Vector3 Angle { get; private set; }

        public void SetScale(float scale)
        {
            if(scale > 0)
            {
                Scale = scale;
            }
        }

        public void SetRotation(Vector3 vec)
        {
            Angle = vec;
        }

        protected virtual void OnAreaChanged()
        {

        }
    }
}
