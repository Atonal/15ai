﻿using HAJE.SlimeAI.Rendering;
using OpenTK.Graphics;
using System.Drawing;
using System.Windows.Forms;

namespace HAJE.SlimeAI.Scene.Component
{
    public class Label : Node
    {
        public Label(string text, Font font)
        {
            this.text = text;
            this.font = font;
            this.fixedArea = false;
            textureDirty = true;
        }

        public Label(string text, Rectangle area, Font font)
        {
            this.text = text;
            this.font = font;
            this.Area = area;
            this.fixedArea = true;
            textureDirty = true;
        }

        public override void Render(Renderer renderer)
        {
            if (textureDirty)
                ReplaceTexture();

            Rectangle area = new Rectangle(0, 0, (int)texture.Size.X, (int)texture.Size.Y);
            if (Alignment == TextAlignment.Left)
            {
                area.X = X;
            }
            else if (Alignment == TextAlignment.Center)
            {
                area.X = X + Width / 2 - area.Width / 2;
            }
            else if (Alignment == TextAlignment.Right)
            {
                area.X = X + Width - area.Width;
            }
            if (VAlignment == TextVAlignment.Bottom)
            {
                area.Y = Y;
            }
            else if (VAlignment == TextVAlignment.Center)
            {
                area.Y = Y + Height / 2 - area.Height / 2;
            }
            else if (VAlignment == TextVAlignment.Top)
            {
                area.Y = Y + Height - area.Height;
            }
            renderer.DrawTexture(texture, area, Color);
        }

        Texture texture;
        void ReplaceTexture()
        {
            textureDirty = false;

            if (texture != null)
            {
                GameSystem.TextureCache.RemoveTexture(texture);
                texture.Dispose();
                texture = null;
            }

            using (Graphics g = Graphics.FromHwnd(GameSystem.MainForm.WindowInfo.Handle))
            {
                SizeF size = g.MeasureString(text, font);
                if (fixedArea)
                    size = g.MeasureString(text, font, Area.Width);

                using (Bitmap bitmap = new Bitmap((int)size.Width, (int)size.Height))
                {
                    using (Graphics gfx = Graphics.FromImage(bitmap))
                    {
                        PointF pt = new PointF(0, 0);
                        gfx.Clear(System.Drawing.Color.Transparent);
                        gfx.DrawString(text, font, Brushes.White,
                            new RectangleF(0, 0, size.Width, size.Height),flag());
                    }

                    texture = Texture.CreateFromBitmap(bitmap);
                }
            }
        }

        Font font;
        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                if (font == value) return;
                font = value;
                textureDirty = true;
            }
        }

        string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                if (string.IsNullOrEmpty(value)) value = " ";
                if (text == value) return;
                text = value;
                textureDirty = true;
            }
        }

        protected override void OnAreaChanged()
        {
            textureDirty = true;
        }

        public Color4 Color = Color4.White;

        bool fixedArea;
        bool textureDirty;

        public TextAlignment Alignment = TextAlignment.Left;
        public TextVAlignment VAlignment = TextVAlignment.Bottom;

        StringFormat flag()
        {
            StringFormat newFlag = new StringFormat();
            if (Alignment == TextAlignment.Left)
                newFlag.Alignment = StringAlignment.Near;
            else if (Alignment == TextAlignment.Center)
                newFlag.Alignment = StringAlignment.Center;
            else newFlag.Alignment = StringAlignment.Far;
            return newFlag;
        }
    }

    public enum TextAlignment
    {
        Left,
        Center,
        Right
    }

    public enum TextVAlignment
    {
        Top,
        Center,
        Bottom
    }
}
