﻿using HAJE.SlimeAI.Rendering;
using OpenTK.Graphics;
using System;
using System.IO;

namespace HAJE.SlimeAI.Scene.Component
{
    public class Sprite : Node
    {
        public Sprite(string path)
        {
            texture = GameSystem.TextureCache.GetTexture(path);
            Width = (int)Math.Ceiling(texture.Size.X);
            Height = (int)Math.Ceiling(texture.Size.Y);
            texturePath = path;
            textureDirty = false;
        }

        public override void Render(Renderer renderer)
        {
            if (textureDirty)
            {
                texture = GameSystem.TextureCache.GetTexture(texturePath);
                textureDirty = false;
            }

            renderer.DrawTexture(texture, Area, Color, 1.0f, Angle, DrawScaleAnchor.M);
        }

        public void UpdatePath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("파일을 찾을 수 없습니다.", path);
            }
            texturePath = path;
            textureDirty = true;
        }

        bool textureDirty;
        string texturePath;
        Texture texture;
        public Color4 Color = Color4.White;
    }
}
