﻿using OpenTK.Graphics;
using System.Drawing;
using System.Drawing.Text;

namespace HAJE.SlimeAI.Scene.Component
{
    public class TimeBar : Node
    {
        public TimeBar(int maxTime, Rectangle area)
        {
            this.Area = area;
            this.maxTime = maxTime;
            this.time = maxTime;
            UpdateTime();
        }

        public override void Render(Rendering.Renderer renderer)
        {
            fill.Render(renderer);
            border.Render(renderer);
            timeLabel.Render(renderer);
        }

        protected override void OnAreaChanged()
        {
            border = new RectangleShape();
            border.Area = this.Area;
            border.BorderThickness = 4;
            border.BorderColor = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            border.FillColor = new Color4(0.0f, 0.0f, 0.0f, 0.0f);

            fill = new RectangleShape();
            fill.Area = new Rectangle(this.Area.X + border.BorderThickness, this.Area.Y + border.BorderThickness,
                this.Area.Width - 2 * border.BorderThickness, this.Area.Height - 2 * border.BorderThickness); 
            fill.BorderThickness = 0;
            fill.FillColor = new Color4(0.57f, 0.82f, 0.31f, 1.0f);
            this.maxWidth = fill.Width;

            Font font = GameSystem.FontManager.GetFont(20);
            timeLabel = new Label("0", new Rectangle(this.Area.X + this.Area.Width / 2 - 100, this.Area.Y + 4, 200, this.Area.Height - 8), font);
            timeLabel.Alignment = TextAlignment.Center;
            timeLabel.VAlignment = TextVAlignment.Top;
        }

        void UpdateTime()
        {
            timeLabel.Text = ((int)this.time).ToString();
            fill.Width = (maxWidth * time) / maxTime;
        }

        int maxTime;
        int time;
        public int Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
                UpdateTime();
            }
        }

        private int maxWidth;

        RectangleShape border;
        RectangleShape fill;
        Label timeLabel;
    }
}
