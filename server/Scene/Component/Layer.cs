﻿using System.Collections.Generic;

namespace HAJE.SlimeAI.Scene.Component
{
    public class Layer : Node
    {
        public override void Render(Rendering.Renderer renderer)
        {
            foreach (var c in children)
            {
                c.Render(renderer);
            }
        }

        public void AddChild(Node child)
        {
            children.Add(child);
        }

        public void RemoveChild(Node child)
        {
            children.Remove(child);
        }

        List<Node> children = new List<Node>();
    }
}
