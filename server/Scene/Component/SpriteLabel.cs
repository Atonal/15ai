﻿using HAJE.SlimeAI.Rendering;
using OpenTK.Graphics;
using System.Drawing;
using System.Windows.Forms;

namespace HAJE.SlimeAI.Scene.Component
{
    public class SpriteLabel : Node
    {
        public SpriteLabel(int textNumber, int size, SpriteFont spriteFont)
        {
            this.textNumber = textNumber;
            this.spriteFont = spriteFont;
            this.fixedArea = false;
            textureDirty = true;
        }

        public override void Render(Renderer renderer)
        {
            if (textureDirty)
                ReplaceTexture();

            Rectangle area = new Rectangle(0, 0, (int)texture.Size.X, (int)texture.Size.Y);
            if (Alignment == TextAlignment.Left)
            {
                area.X = X;
            }
            else if (Alignment == TextAlignment.Center)
            {
                area.X = X + Width / 2 - area.Width / 2;
            }
            else if (Alignment == TextAlignment.Right)
            {
                area.X = X + Width - area.Width;
            }
            if (VAlignment == TextVAlignment.Bottom)
            {
                area.Y = Y;
            }
            else if (VAlignment == TextVAlignment.Center)
            {
                area.Y = Y + Height / 2 - area.Height / 2;
            }
            else if (VAlignment == TextVAlignment.Top)
            {
                area.Y = Y + Height - area.Height;
            }
            renderer.DrawTexture(texture, area, Color);
        }

        Texture texture;
        SpriteFont spriteFont;

        void ReplaceTexture()
        {
            textureDirty = false;

            if (texture != null)
            {
                GameSystem.TextureCache.RemoveTexture(texture);
                texture.Dispose();
                texture = null;
            }


            SizeF size = spriteFont.MeasureString(textNumber, this.size);
            if (fixedArea)
                size = spriteFont.MeasureString(textNumber, this.size, Area.Width);

            using (Bitmap bitmap = new Bitmap((int)size.Width, (int)size.Height))
            {
                spriteFont.makeTexture(textNumber, this.size, bitmap);
                using (Graphics gfx = Graphics.FromImage(bitmap))
                {
                    //PointF pt = new PointF(0, 0);
                    //gfx.Clear(System.Drawing.Color.Transparent);
                    //gfx.DrawString(text, font, Brushes.White,
                    //    new RectangleF(0, 0, size.Width, size.Height), flag());
                }

                texture = Texture.CreateFromBitmap(bitmap);
            }
        }
        public SpriteFont SpriteFont
        {
            get
            {
                return spriteFont;
            }
            set
            {
                if (spriteFont == value) return;
                spriteFont = value;
                textureDirty = true;
            }
        }

        int textNumber;
        public int TextNumber
        {
            get
            {
                return textNumber;
            }
            set
            {
                if (textNumber == value) return;
                textNumber = value;
                textureDirty = true;
            }
        }

        int size;
        public int Size
        {
            get
            {
                return size;
            }
            set
            {
                if (size == value) return;
                size = value;
                textureDirty = true;
            }
        }

        protected override void OnAreaChanged()
        {
            textureDirty = true;
        }

        public Color4 Color = Color4.White;

        bool fixedArea;
        bool textureDirty;

        public TextAlignment Alignment = TextAlignment.Left;
        public TextVAlignment VAlignment = TextVAlignment.Bottom;

        StringFormat flag()
        {
            StringFormat newFlag = new StringFormat();
            if (Alignment == TextAlignment.Left)
                newFlag.Alignment = StringAlignment.Near;
            else if (Alignment == TextAlignment.Center)
                newFlag.Alignment = StringAlignment.Center;
            else newFlag.Alignment = StringAlignment.Far;
            return newFlag;
        }
    }
}
