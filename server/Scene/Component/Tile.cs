﻿using HAJE.SlimeAI.Rendering;
using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections;
using System.Collections.Generic;

namespace HAJE.SlimeAI.Scene.Component
{
    public enum TileEffectState
    {
        None,
        Normal,
        Flip,
    }

    public enum TileColorState
    {
        red,
        blue,
        none,
    }
    public class Tile : Node
    {
        public Tile()
        {
            texture = new Texture[] {null, null, null};
            currentTileBaseColorState = TileColorState.none;
            currentTileEffectColorState = TileColorState.none;
        }

        public void Update(float updateInterval)
        {
            if(effectTimer > 0)
            {
                if(delayTimer > 0)
                {
                    delayTimer -= updateInterval;
                    if (delayTimer < 0)
                        delayTimer = 0;
                }
                else
                {
                    effectTimer -= updateInterval;
                    if (effectTimer <= 0)
                    {
                        effectTimer = 0;
                        DecoratedEndHandler();
                    }  
                }
            }
        }

        public void SetColorChangeEffect(TileEffectState effectType, int newState, float effectTime, float delayTime = 0f)
        {
            /*
            if (state == 0) effectColor = redTeamTileColor;
            else if (state == 1) effectColor = blueTeamTileColor;
            else effectColor = defaultTileColor;

            state = newState;
            maxEffectTime = effectTime;
            effectTimer = effectTime;

            if (state == 0) baseColor = redTeamTileColor;
            else if (state == 1) baseColor = blueTeamTileColor;
            else baseColor = defaultTileColor;
            */

            if (state == 0) currentTileEffectColorState = TileColorState.red;
            else if (state == 1) currentTileEffectColorState = TileColorState.blue;
            else currentTileEffectColorState = TileColorState.none;

            state = newState;
            maxEffectTime = effectTime;
            effectTimer = effectTime;

            if (state == 0) currentTileBaseColorState = TileColorState.red;
            else if (state == 1) currentTileBaseColorState = TileColorState.blue;
            else currentTileBaseColorState = TileColorState.none;

            delayTimer = delayTime;
            currentTileEffect = effectType;
        }

        public override void Render(Renderer renderer)
        {
            if(effectTimer > 0)
            {
                if (currentTileEffect == TileEffectState.Flip)
                {
                    if(delayTimer >  0)
                    {
                        //딜레이 중엔 이전 스테이트 색이 보이도록
                        //renderer.DrawTexture(texture, Area, effectColor);
                        renderer.DrawTexture(GetTexture(currentTileEffectColorState), Area, Color4.White);
                    }
                    else
                    {
                        renderer.DrawTexture(GetTexture(currentTileBaseColorState), Area, Color4.White);
                        renderer.DrawTexture(GetTexture(currentTileEffectColorState), Area, new Color4(1f, 1f, 1f, effectTimer / maxEffectTime));
                        /*
                        renderer.DrawTexture(GetTexture(currentTileBaseColorState), Area, Color4.White);
                        renderer.DrawTexture(GetTexture(TileColorState.none), Area, new Color4(1f, 1f, 1f, effectTimer / maxEffectTime));
                         * */
                    }
                    //현재 스테이트 색으로 맞춘 다음 알파로 점점 보여지게 
                }
                else
                {
                    renderer.DrawTexture(GetTexture(currentTileEffectColorState), Area, Color4.White);
                    //실제 현재 스테이트 색으로 맞춰지게 하기 위해 baseColor를 점점 커지는 형태로
                    renderer.DrawTexture(GetTexture(currentTileBaseColorState), Area, Color4.White, 1f - (effectTimer / maxEffectTime), DrawScaleAnchor.M);
                    
                }
            }
            else
            {
                renderer.DrawTexture(GetTexture(currentTileBaseColorState), Area, Color4.White);
            }
        }

        Texture GetTexture(TileColorState tileColor)
        {
            if(texture[(int)tileColor] == null)
            {
                switch(tileColor)
                {
                    case TileColorState.red:
                        texture[(int)tileColor] = GameSystem.TextureCache.GetTexture("Resource/TileRed.png");
                        break;
                    case TileColorState.blue:
                        texture[(int)tileColor] = GameSystem.TextureCache.GetTexture("Resource/TileBlue.png");
                        break;
                    case TileColorState.none:
                        texture[(int)tileColor] = GameSystem.TextureCache.GetTexture("Resource/TileWhite.png");
                        break;
                }
            }

            return texture[(int)tileColor];
        }


        float effectTimer;
        float maxEffectTime;
        float delayTimer;

        Texture[] texture;
        TileColorState currentTileBaseColorState;
        TileColorState currentTileEffectColorState;
        

        Color4 effectColor = Color4.White;
        Color4 baseColor = Color4.White;

        public BitArray edgeInfo = new BitArray(4,false);
        TileEffectState currentTileEffect = TileEffectState.None;
        

        int state;
        public int State
        {
            get
            {
                return state;
            }

            set
            {
                state = value;
                if (state == 0) currentTileBaseColorState = TileColorState.red;
                else if (state == 1) currentTileBaseColorState = TileColorState.blue;
                else currentTileBaseColorState = TileColorState.none;
            }
        }

        readonly Color4 redTeamTileColor = new Color4(0.95f, 0.49f, 0.41f, 1.0f);
        readonly Color4 blueTeamTileColor = new Color4(0.26f, 0.88f, 1.0f, 1.0f);
        readonly Color4 defaultTileColor = new Color4(0.5f, 0.5f, 0.5f, 0.5f);

        void DecoratedEndHandler()
        {
            EndHandler += (self) => { EndHandler = (t) => { }; };
            EndHandler(this);
        }
        public event TileEffectEndHandler EndHandler = (self) => { };
    }
    public delegate void TileEffectEndHandler(Tile self);
}
