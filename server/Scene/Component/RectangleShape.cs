﻿using HAJE.SlimeAI.Rendering;
using OpenTK.Graphics;
using System.Drawing;

namespace HAJE.SlimeAI.Scene.Component
{
    public class RectangleShape : Node
    {
        public RectangleShape()
        {
            texture = GameSystem.TextureCache.GetTexture("Resource/white.png");
        }

        public override void Render(Renderer renderer)
        {
            Rectangle leftArea = new Rectangle(Area.X, Area.Y, BorderThickness, Area.Height - BorderThickness);
            renderer.DrawTexture(texture, leftArea, BorderColor);

            Rectangle rightArea = new Rectangle(Area.Right - BorderThickness, Area.Top + BorderThickness, BorderThickness, Area.Height - BorderThickness);
            renderer.DrawTexture(texture, rightArea, BorderColor);

            Rectangle topArea = new Rectangle(Area.X + BorderThickness, Area.Y, Area.Width - BorderThickness, BorderThickness);
            renderer.DrawTexture(texture, topArea, BorderColor);

            Rectangle bottomArea = new Rectangle(Area.Left, Area.Bottom - BorderThickness, Area.Width - BorderThickness, BorderThickness);
            renderer.DrawTexture(texture, bottomArea, BorderColor);

            Rectangle fillArea = new Rectangle(Area.X + BorderThickness, Area.Y + BorderThickness, Area.Width - 2 * BorderThickness, Area.Height - 2 * BorderThickness);
            renderer.DrawTexture(texture, fillArea, FillColor);
        }

        public int BorderThickness;
        public Color4 BorderColor;
        public Color4 FillColor;
        Texture texture;
    }
}
