﻿using HAJE.SlimeAI.Rendering;
using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections.Generic;

namespace HAJE.SlimeAI.Scene.Component
{
    public enum MoveSpeedType
    {
        linear,
        concave_curve,
        convex_curve,
    }
    public class Animation : Node
    {
        public Animation(string path, int maxFrame)
        {
            texture = GameSystem.TextureCache.GetTexture(path);
            Width = (int)Math.Ceiling(texture.Size.X) / maxFrame;
            Height = (int)Math.Ceiling(texture.Size.Y);
            texturePath = path;
            textureDirty = false;

            this.currentElapsedTime = 0f;//애니메이션 재생 시간, 프레임이 넘어가면 0으로 초기화
            this.intervalTime = 0.25f; //애니메이션 프레임간 간격 시간. 단위:초 
            this.currentFrame = 0; //현재 프레임
            this.maxFrame = maxFrame;

            //각 애니메이션 프레임이 한줄로 왼쪽부터 차례대로 이어진다고 가정
            //TODO : UVRect를 직접 설정할 수 있도록 변경
            float frameUVLength = 1.0f / (float)maxFrame;
            animationFrameUVRect.Clear();
            for(int i=0; i<maxFrame; i++)
                animationFrameUVRect.Add(new RectangleF(i*frameUVLength,0,frameUVLength,1));

            this.moveTime = 0;
            this.currentTargetPosition = Point.Empty;
        }

        public override void Render(Renderer renderer)
        {
            if (textureDirty)
            {
                texture = GameSystem.TextureCache.GetTexture(texturePath);
                textureDirty = false;
            }
            renderer.DrawTexture(texture, Area, animationFrameUVRect[currentFrame%maxFrame],Color);
        }

        public void Update()
        {

        }

        public void Update(float updateInterval)
        {
            currentElapsedTime += updateInterval;
            if (currentElapsedTime > intervalTime)
            {
                currentElapsedTime -= intervalTime;
                currentFrame++;
            }

            if(moveTime > 0)
            {
                moveTime -= updateInterval;
                if (moveTime <= 0)
                {//설정한 시간이 끝나면 목표 지점으로
                    Area = new Rectangle(Point.Truncate(currentTargetPosition), Area.Size);
                }
                else
                {
                    currentPosition.X = (float)(currentPosition.X + velocity.X * updateInterval);
                    currentPosition.Y = (float)(currentPosition.Y + velocity.Y * updateInterval);

                    Area = new Rectangle(new Point((int)currentPosition.X, (int)currentPosition.Y), Area.Size);
                    velocity = new PointF((float)(velocity.X + accel.X*updateInterval), (float)(velocity.Y + accel.Y*updateInterval));
                }
            }

            //Area = new Rectangle(currentTargetPosition, Area.Size);
           
        }

        public void UpdatePath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("파일을 찾을 수 없습니다.", path);
            }
            texturePath = path;
            textureDirty = true;
        }

        public void Move(PointF targetPosition, MoveSpeedType moveType = MoveSpeedType.linear, float seconds = 1f)
        {
            moveTime = seconds; //설정한 시간이 지나면 도착하도록

            if(!currentTargetPosition.IsEmpty)
                Area = new Rectangle(Point.Truncate(currentTargetPosition), Area.Size);

            currentPosition = Area.Location;
            currentTargetPosition = targetPosition;

            switch(moveType)
            {
                case MoveSpeedType.linear: //등속
                    velocity = new PointF((targetPosition.X - Area.X) / seconds, (targetPosition.Y - Area.Y) / seconds);
                    accel = new PointF(0, 0);
                    break;
                case MoveSpeedType.convex_curve: //가속
                    velocity = new PointF(0, 0);
                    accel = new PointF((targetPosition.X - Area.X) * 2 / seconds, (targetPosition.Y - Area.Y) * 2 / seconds);
                    break;
                case MoveSpeedType.concave_curve: //감속
                default:
                    velocity = new PointF((targetPosition.X - Area.X) * 2 / seconds, (targetPosition.Y - Area.Y) * 2 );
                    accel = new PointF(-velocity.X, -velocity.Y);
                    break;
            }
        }

        public void PositionSet(Point newPos)
        {
            Area = new Rectangle(newPos, Area.Size);
            currentPosition = newPos;
        }


        bool textureDirty;
        string texturePath;
        Texture texture;
        public Color4 Color = Color4.White;

        float currentElapsedTime;
        float intervalTime;
        int currentFrame;

        //TODO : 얌전히 vector를 씁시다
        PointF currentTargetPosition;
        PointF currentPosition;
        PointF velocity;
        PointF accel;
        double moveTime;

        int maxFrame;


        List<RectangleF> animationFrameUVRect = new List<RectangleF>();

    }
}
