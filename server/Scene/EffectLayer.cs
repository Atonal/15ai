﻿using HAJE.SlimeAI.Scene.Component;
using HAJE.SlimeAI.Rendering;

using OpenTK.Graphics;
using System;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Collections;
using System.Collections.Generic;

namespace HAJE.SlimeAI.Scene
{
    public class EffectLayer : Layer
    {
        public EffectLayer()
        {

        }

        public void UpdateGameState(float elapsedTime)
        {
            for (int i = 0; i < effectList.Count; i++)
            {
                if (!effectList[i].IsFinished())
                    effectList[i].Update(elapsedTime);
            }
        }

        public void AddTileFlipBorderEffect(float effectTime, float delayTime, Rectangle area, BitArray edgeInfo)
        {   
            
            Effect newEffect = null;
            foreach(Effect ef in effectList)
            {
                if (ef is EffectOutline && ef.IsFinished())
                {
                   newEffect = ef;
                   break;
                }
            }

            if (newEffect == null)
            {
                //List에 재사용 가능한 TileFlipBorder 클래스가 없으면 새로 만든다.
                newEffect = new EffectOutline(effectTime, delayTime, area, edgeInfo);
                effectList.Add(newEffect);
            }
            else
            {
                //List에 재사용 가능한 TileFlipBorder 클래스가 존재하면 재사용
                EffectOutline outlineEffect = newEffect as EffectOutline;
                outlineEffect.InitializeInfo(effectTime, delayTime, area, edgeInfo);
            }
        }

        public void AddTileFlipParticleEffect(Rectangle area, int team, int particleNum,float delayTime = 0f)
        {
            Effect newEffect = null;
            foreach (Effect ef in effectList)
            {
                if (ef is EffectFlipParticle && ef.IsFinished())
                {
                    newEffect = ef;
                    break;
                }
            }

            if (newEffect == null)
            {
                //List에 재사용 가능한 EffectOutline 클래스가 없으면 새로 만든다.
                newEffect = new EffectFlipParticle(team, particleNum, area, delayTime, 1.0f);
                effectList.Add(newEffect);
            }
            else
            {
                //List에 재사용 가능한 EffectOutline 클래스가 존재하면 재사용
                EffectFlipParticle particleEffect = newEffect as EffectFlipParticle;
                particleEffect.AddParticle(team, particleNum, area, 1.0f);
                particleEffect.SetDelayTime(delayTime);
            }
        }

        public void SetGameResultScreen(Rectangle area, int winningTeam, string teamName, float seconds = 1f, float delay = 0f)
        {
            Effect newEffect = new EffectGameResult(winningTeam, area, teamName, seconds, delay+seconds*0.5f);
            effectList.Add(newEffect);
        }

        public void SetCountdown(Rectangle area, int count)
        {
            Effect newEffect = new EffectCountdown(count, area, 1f);
            effectList.Add(newEffect);
        }

        public void SetGameover(Rectangle area)
        {
            Effect newEffect = new EffectGameover(area, 4f);
            effectList.Add(newEffect);
        }

        public override void Render(Rendering.Renderer renderer)
        {
            for (int i = 0; i < effectList.Count; i++)
            {
                effectList[i].Render(renderer);
            }
        }

        List<Effect> effectList =  new List<Effect>();
    }

    
}
