﻿using HAJE.SlimeAI.Rendering;
using HAJE.SlimeAI.Scene.Component;
using OpenTK.Graphics;
using System.Drawing;
using System.Drawing.Text;

namespace HAJE.SlimeAI.Scene
{
    public class GameScene
    {
        public GameScene(int width, int height)
        {
            var creator = new Sprite("Resource/creator.png");
            creator.Height = (int)(height * 0.03);
            creator.Width = creator.Height * 8;
            creator.Right = width/2 + creator.Width/2;
            creator.Bottom = 10;
            rootLayer.AddChild(creator);

            //게임판 
            Rectangle battleArea = new Rectangle(4, creator.Height + 12, width - 8, (int)(height * 0.7));
            battleGround = new BattleGroundLayer(battleArea);
            battleGround.Area = battleArea;
            rootLayer.AddChild(battleGround);

            //게임 정보 UI
            ui = new UILayer(battleGround.Area, width , height);
            ui.Area = new Rectangle();
            rootLayer.AddChild(ui);
        }

        public void Render(Renderer renderer)
        {
            rootLayer.Render(GameSystem.Renderer);
        }

        public void SetTeamName(int teamIndex, string teamName)
        {
            ui.SetTeamName(teamIndex, teamName);
          
        }

        public void SetTeamMessage(int teamIndex, string message)
        {
            ui.SetTeamMessage(teamIndex, message);
        }

        public void SetTeamScore(int teamIndex, int score)
        {
            ui.SetTeamScore(teamIndex, score);
        }

        public void SetTeamAccumulateScore(int teamIndex, int accScore)
        {
            ui.SetTeamAccumulateScore(teamIndex, accScore);
        }

        public void SetTeamSlimeDeath(int teamIndex, int death)
        {
            ui.SetTeamSlimeDeath(teamIndex, death);
        }

        public void SetTime(int time)
        {
            ui.SetTime(time);
        }

        public void SetGameResult(int winningTeam, int[] TeamScore, float seconds)
        {
            battleGround.SetGameResult(winningTeam,ui.GetTeamName(winningTeam),TeamScore, seconds);
        }

        public void SetCurrentWinningTeam(int winningTeam)
        {
            ui.SetCurrentWinningTeam(winningTeam);
        }

        public void Update(float elapsedTime)
        {
            battleGround.UpdateGameState(elapsedTime);
        }

        public void Initialize()
        {
            for (int iT = 0; iT < Logic.Team.Max; iT++)
            {
                SetTeamAccumulateScore(iT, 0);
                SetTeamMessage(iT, "시간과 예산을 더 주신다면");
                SetTeamName(iT, "AI_Team" + iT.ToString());
                SetTeamScore(iT, 0);
                SetTeamSlimeDeath(iT, 0);
                SetTime(Logic.Simulation.MaxTurn);

            }
        }

        public void SetGameState(Point[][] slimePos, int[,] boardState, int[,] flipState, float seconds)
        {
            battleGround.SetGameState(slimePos, boardState, flipState, seconds);
        }

        public void SetCountdown(int count)
        {
            battleGround.SetCountdown(count);
        }

        public void SetGameover()
        {
            battleGround.SetGameover();
        }

        Layer rootLayer = new Layer();

        BattleGroundLayer battleGround;
        UILayer ui;
    }
}
