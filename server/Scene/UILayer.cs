﻿using HAJE.SlimeAI.Logic;
using HAJE.SlimeAI.Scene.Component;
using OpenTK.Graphics;
using System.Drawing;
using System.Drawing.Text;
using System.Timers;

namespace HAJE.SlimeAI.Scene
{

    public class UILayer : Layer
    {
        public UILayer(Rectangle battleGround, int width, int height)
        {
            //남은 시간을 표시해주는 Bar
            timeBar = new TimeBar(Logic.Simulation.MaxTurn,
                new Rectangle(battleGround.Left, battleGround.Bottom + 5, battleGround.Width, (int)(height * 0.05)));
            base.AddChild(timeBar);

            Font[] fonts = { GameSystem.FontManager.GetFont(36), 
                             GameSystem.FontManager.GetFont(18),
                             GameSystem.FontManager.GetFont(14)};

            #region 팀 정보
            Rectangle redTeamBox = new Rectangle(timeBar.Area.Left, timeBar.Area.Bottom + 5, battleGround.Width / 2 - 31, (int)(height * 0.19));
            Rectangle blueTeamBox = new Rectangle(redTeamBox.Left + redTeamBox.Width + 63, redTeamBox.Top, redTeamBox.Width, redTeamBox.Height);
            teamInfo = new TeamInfomationAreaLayer[] {   new TeamInfomationAreaLayer(0, redTeamBox    ,fonts), 
                                                    new TeamInfomationAreaLayer(1, blueTeamBox   ,fonts) };
            base.AddChild(teamInfo[0]);
            base.AddChild(teamInfo[1]);
            #endregion

            #region 슬라임 사망 이미지
            //임시 이미지
            slimeDeadImage = new Sprite("Resource/dead.png");
            slimeDeadImage.Height = 30;
            slimeDeadImage.Width = slimeDeadImage.Height;
            slimeDeadImage.Left = timeBar.Area.Left + timeBar.Width / 2 - slimeDeadImage.Width / 2;
            slimeDeadImage.Top = redTeamBox.Bottom - 13;
            base.AddChild(slimeDeadImage);
            #endregion

            #region 누적 점수 이미지
            //이것도 임시 이미지
            accumulateScoreImage = new Sprite("Resource/plus.png");
            accumulateScoreImage.Height = 30;
            accumulateScoreImage.Width = slimeDeadImage.Height;
            accumulateScoreImage.Left = slimeDeadImage.Left;
            accumulateScoreImage.Top = slimeDeadImage.Bottom - 3;
            base.AddChild(accumulateScoreImage);
            #endregion

            #region 승패 상황
            inequalImage = new Sprite("Resource/ineq.png");
            inequalImage.SetRotation(new OpenTK.Vector3(0.0f, 90.0f, 0.0f));
            inequalImage.Color.A = 0.0f;
            inequalImage.Height = 48;
            inequalImage.Width = 48;
            inequalImage.Left = timeBar.Area.Left + timeBar.Width / 2 - inequalImage.Width / 2;
            inequalImage.Top = accumulateScoreImage.Bottom - 8;
            base.AddChild(inequalImage);

            equalImage = new Sprite("Resource/eq.png");
            equalImage.Height = 48;
            equalImage.Width = 48;
            equalImage.Left = timeBar.Area.Left + timeBar.Width / 2 - equalImage.Width / 2;
            equalImage.Top = accumulateScoreImage.Bottom - 8;
            base.AddChild(equalImage);

            #endregion

        }
        public override void Render(Rendering.Renderer renderer)
        {
            base.Render(renderer);
        }

        public void SetTeamName(int teamIndex, string teamName)
        {
            this.teamInfo[teamIndex].SetTeamName(teamName);
        }

        public string GetTeamName(int teamIndex)
        {
            return this.teamInfo[teamIndex].GetTeamName();
        }

        public void SetTeamMessage(int teamIndex, string message)
        {
            this.teamInfo[teamIndex].SetTeamMessage(message);
        }

        public void SetTeamScore(int teamIndex, int score)
        {
            this.teamInfo[teamIndex].SetTeamScore(score);
        }

        public void SetTeamAccumulateScore(int teamIndex, int accScore)
        {
            this.teamInfo[teamIndex].SetTeamAccumulateScore( accScore );
        }

        public void SetTeamSlimeDeath(int teamIndex, int death)
        {
            this.teamInfo[teamIndex].SetTeamSlimeDeath(death);
        }

        public void SetTime(int time)
        {
            timeBar.Time = time;
        }

        public void SetCurrentWinningTeam(int winningTeam)
        {
            if (winningTeam == Team.Neutral)
            {
                equalImage.Color.A = 1.0f;
                inequalImage.Color.A = 0.0f;
            }
            else
            {
                equalImage.Color.A = 0.0f;
                inequalImage.Color.A = 1.0f;
                if (winningTeam == Team.Red) inequalImage.SetRotation(new OpenTK.Vector3(0.0f, 180.0f, 0.0f));
                else inequalImage.SetRotation(new OpenTK.Vector3(0.0f, 0.0f, 0.0f));
            }
        }

        TeamInfomationAreaLayer[] teamInfo;
        TimeBar timeBar;
        Sprite slimeDeadImage;
        Sprite accumulateScoreImage;
        Sprite inequalImage;
        Sprite equalImage;
    }
}
