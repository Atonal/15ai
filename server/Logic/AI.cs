﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.SlimeAI.Logic
{
    public class AI
    {
        private int team;
        private static int turn;
        private Point[][] Locations;
        private int[,] BoardState;


        AI()
        //AI 초기화
        {
            Locations = new Point[2][];
            Locations[0] = new Point[5];
            Locations[1] = new Point[5];
            BoardState = new int[21, 11];
        }
        public void setTeam(byte team)
        {
            this.team = team;
        }

        public byte[] sendmsg(String s)
        //팀 메시지 결정
        //40자 이상이 되면 뒤에 짤림
        {
            byte[] result = new byte[41];
            result[0] = 255;
            char[] temp = s.ToCharArray();

            for (int i = 1; i < 41; i++)
            {
                result[i] = (byte)temp[i];
            }
            return result;
        }
        private void readData(byte[] data)
        //게임 진행중 턴마다 받을 data
        //지금은 게임판 size가 21*11이라고 가정해 놓음
        {
            //read turn
            AI.turn = data[0] * 256 + data[1];
            
            //read board state
            int i = 0;
            for (i = 2; i < data.Length - 20; i++)
            {
                BoardState[(i-2) / 11, (i-2) % 11] = data[i];
                if (data[i] == 255) BoardState[(i - 2) / 11, (i - 2) % 11] = Team.Neutral;
            }

            //read slime location
            
            int team = 0;
            int slimeno = 0;
            for (; i < data.Length;i+=2 )
            {
                Locations[team][slimeno].X = data[i];
                Locations[team][slimeno].Y = data[i + 1];
                slimeno++;
                if (slimeno == 5)
                {
                    slimeno = 0;
                    team++;
                }
            }
        }
        private Direction[] run()
        //슬라임이 어떻게 움직일지를 Direction[]의 형태로 return합니다.
        //메인 코드
        //기본 버전 : 모든 슬라임이 랜덤하게 움직입니다
        {
            Direction[] result = new Direction[5];
            Random r = new Random();
            Direction[] templist = new Direction[4]{Direction.Up,Direction.Left,Direction.Right,Direction.Down};
            for (int i = 0; i < 5; i++)
            {
                result[i] = templist[r.Next(4)];
            }
            return result;
        }
        private byte[] writeData(Direction[] d)
        //null의 경우 아래 방향으로 움직임
        {
            byte[] result = new byte[5];
            for (int i = 0; i < 5; i++)
            {
                if (d[i] == Direction.Right) result[i] = 0;
                else if (d[i] == Direction.Up) result[i] = 1;
                else if (d[i] == Direction.Left) result[i] = 2;
                else result[i] = 3;
            }
            return result;
        }
        public byte[] main(byte[] data)
        //서버 or 클라에서 부르는 함수입니다.
        {
            readData(data);
            return writeData(run());
        }
       
    }
}
