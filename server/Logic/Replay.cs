﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.SlimeAI.Logic
{
    public class Replay
    {
        //public const string replayFliePath;
        public const string loadFilePath = "./record.txt";
        public const string saveDirectory = "./Records";
        private string saveFilePath = "./Records/record" + System.DateTime.Now.ToString("_yyMMdd-hhmmss") + ".txt";
        public Replay()
        {
        }

        public void Load()
        {
            if (File.Exists(loadFilePath))
            {
                using (StreamReader sr = File.OpenText(loadFilePath))
                {
                    string s = "";
                    string[] sl = { };
                    if ((s = sr.ReadLine()) != null)
                        sl = s.Split(new Char[] { ',', ' ', '\t' });
                    int team = Convert.ToInt32(sl[0]);
                    int slime = Convert.ToInt32(sl[1]);
                    int turn = Convert.ToInt32(sl[2]);
                    string[] teamNames = new string[team];

                    for (int iT = 0; iT < team; iT++)
                    {
                        teamNames[iT] = sr.ReadLine();
                    }
                    Clear(teamNames);

                    for (int i = 0; i < turn; i++)
                    {
                        Direction[,] order = new Direction[team, slime];
                        for (int iT = 0; iT < team; iT++)
                        {
                            if ((s = sr.ReadLine()) != null)
                                sl = s.Split(new Char[] { ',', ' ', '\t' });
                            Debug.Assert(sl.Length >= slime);
                            for (int iS = 0; iS < slime; iS++)
                            {
                                string o = sl[iS];
                                Direction d;
                                if ("D"==o) d = Direction.Down;
                                else if ("L"==o) d = Direction.Left;
                                else if ("R"==o) d = Direction.Right;
                                else d = Direction.Up;
                                order[iT, iS] = d;
                            }

                        }
                        Record(i, order);

                    }

                }
            }
        }

        public void Save()
        {
            int team = TeamNames.GetLength(0);
            int slime = Inputs[0].GetLength(1);
            int turn = Inputs.Count;
            using (StreamWriter file = new StreamWriter(loadFilePath, false))
            {
                file.WriteLine(String.Join(" ", new[]{team, slime, turn}));
                for (int iT = 0; iT < team; iT++)
                {
                    file.WriteLine(TeamNames[iT]);
                }
                for (int i = 0; i < turn; i++)
                {
                    Direction[,] order = Inputs[i];
                    for (int iT = 0; iT < team; iT++)
                    {
                        var s= new string [slime];
                        for (int iS = 0; iS < slime; iS++)
                        {
                            string o;
                            Direction d = order[iT, iS];
                            if (d == Direction.Down) o = "D";
                            else if (d == Direction.Left) o = "L";
                            else if (d == Direction.Right) o = "R";
                            else o = "U";
                            s[iS] = o;
                        }
                        file.WriteLine(String.Join(" ",s));
                    }

                }
            }
            System.IO.Directory.CreateDirectory(saveDirectory);
            File.Copy(loadFilePath, saveFilePath, true);

        }

        public void Clear(string[] teamNames)
        {
            Debug.Assert(teamNames.Length == Team.Max);
            for (int i = 0; i < Team.Max; i++)
            {
                TeamNames[i] = teamNames[i];
            }
            Inputs.Clear();
        }

        public void Record(int turn, Direction[,] order)
        {
            Debug.Assert(turn == Inputs.Count);
            Direction[,] orderCopy = new Direction[order.GetLength(0), order.GetLength(1)];
            for (int iT = 0; iT < order.GetLength(0); iT++)
                for (int iS = 0; iS < order.GetLength(1); iS++)
                    orderCopy[iT, iS] = order[iT, iS];
            Inputs.Add(orderCopy);

        }

        public string[] TeamNames = new string[Team.Max];
        public List<Direction[,]> Inputs = new List<Direction[,]>();

        public Direction[,] GetTurnInput(int p)
        {
            try
            {
                return Inputs[p];
            }
            catch (ArgumentOutOfRangeException)
            {
                return Inputs.Last();
            }
            throw new NotImplementedException();
        }
    }
}
