﻿namespace HAJE.SlimeAI.Logic
{
    public enum Direction
    {
        Default = -1,
        Up = 0,
        Down,
        Left,
        Right,
    }
}
