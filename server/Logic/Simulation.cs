﻿using System.Collections.Generic;
using System.Drawing;

namespace HAJE.SlimeAI.Logic
{
    public class Simulation
    {
        public const int MaxSlime = 5;
        public const int Width = 25;
        public const int Height = 13;
        public const int MaxTurn = 180;
        public const double TurnLength = 0.6667f; // 한 턴의 길이 (단위: 초)

        public int[,] BoardState = new int[Width, Height]; // [X, Y]
        public Point[] RespawnPoint;  // 죽은 슬라임이 태어나는 지점의 위치 [Team]
        public Point[][] SlimePosition; // [Team, SlimeIndex]
        public Point[] RedSlimes;   // [SlimeIndex]
        public Point[] BlueSlimes;  // [SlimeIndex]

        public int[,] FlipState = new int[Width, Height];

        public int[] Score = new int[Team.Max];
        public int[] TotalDeadSlimes = new int[Team.Max];
        public int[] AccScore = new int[Team.Max];
        public int CurrentTurn = 0;
        public Direction[] InitialDirection;  // 기본 슬라임 방향 [Team]
        public Direction[,] LastOrder = new Direction[Team.Max, MaxSlime];

        public int CurrentWinningTeam = Team.Neutral;

        public Simulation()
        {
            SlimePosition = new Point[Team.Max][];
            RedSlimes = new Point[MaxSlime];
            BlueSlimes = new Point[MaxSlime];
            SlimePosition[Team.Red] = RedSlimes;
            SlimePosition[Team.Blue] = BlueSlimes;

            // 초기 슬라임 배치 (원점은 왼쪽 아래)
            RedSlimes[0] = new Point(0, Height - 1);
            RedSlimes[1] = new Point(0, Height / 2);
            RedSlimes[2] = new Point(0, 0);
            RedSlimes[3] = new Point(Width / 4, Height / 4);
            RedSlimes[4] = new Point(Width / 4, Height - 1 - Height / 4);

            BlueSlimes[0] = new Point(Width - 1, Height - 1);
            BlueSlimes[1] = new Point(Width - 1, Height / 2);
            BlueSlimes[2] = new Point(Width - 1, 0);
            BlueSlimes[3] = new Point(Width - 1 - Width / 4, Height / 4);
            BlueSlimes[4] = new Point(Width - 1 - Width / 4, Height - 1 - Height / 4);

            // 초기 보드 상태
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                    BoardState[x, y] = Team.Neutral;

            // 리스폰 위치 초기화
            RespawnPoint = new Point[Team.Max];
            RespawnPoint[Team.Red] = new Point(0, 0);
            RespawnPoint[Team.Blue] = new Point(Width - 1, Height - 1);

            // 초기 슬라임 방향 초기화 (첫턴에 order가 들어오지 않을 경우 대비)
            InitialDirection = new Direction[Team.Max];
            InitialDirection[Team.Red] = Direction.Left;
            InitialDirection[Team.Blue] = Direction.Right;
            for (int i = 0; i < MaxSlime; i++)
            {
                LastOrder[Team.Red, i] = InitialDirection[Team.Red];
                LastOrder[Team.Blue, i] = InitialDirection[Team.Blue];
            }

            // 초기 슬라임 위치의 색깔 칠하기
            for (int i = 0; i < MaxSlime; i++)
            {
                Point p = RedSlimes[i];
                BoardState[p.X, p.Y] = Team.Red;
                p = BlueSlimes[i];
                BoardState[p.X, p.Y] = Team.Blue;
            }

            // 초기 양팀 점수 초기화
            ScoreInitialize();

        }

        private void ScoreInitialize()
        {
            for (int iT = 0; iT < Team.Max; iT++)
            {
                Score[iT] = 5;
                TotalDeadSlimes[iT] = 0;
                AccScore[iT] = 0;
            }
        }

        /// <summary>
        /// 각 턴의 입력 결과를 받아 시뮬레이션을 업데이트
        /// </summary>
        /// <param name="order">각 팀별 입력 [Team, SlimeIndex]</param>
        public void NextTurn(Direction[,] order)
        {

            var _SlimePosition = new Point[Team.Max][];
            var _RedSlimes = new Point[MaxSlime];
            var _BlueSlimes = new Point[MaxSlime];
            _SlimePosition[Team.Red] = _RedSlimes;
            _SlimePosition[Team.Blue] = _BlueSlimes;

            var CollidedSlimes = new bool[Team.Max][];
            var CollidedRed = new bool[MaxSlime]; // all False
            var CollidedBlue = new bool[MaxSlime]; // all False
            var _CollidedRed = new bool[MaxSlime]; // all False
            var _CollidedBlue = new bool[MaxSlime]; // all False
            CollidedSlimes[Team.Red] = CollidedRed;
            CollidedSlimes[Team.Blue] = CollidedBlue;


            // 슬라임의 다음 위치 계산 (order 유효화 됨)
            for (int i = 0; i < MaxSlime; i++)
            {
                _RedSlimes[i] = NextPosition(RedSlimes[i], LastOrder[Team.Red, i], ref order[Team.Red, i]);
                _BlueSlimes[i] = NextPosition(BlueSlimes[i], LastOrder[Team.Blue, i], ref order[Team.Blue, i]);
            }

            for (int r = 0; r < MaxSlime; r++)
                for (int b = 0; b < MaxSlime; b++)
                {
                    if ((_RedSlimes[r] == BlueSlimes[b]) && (RedSlimes[r] == _BlueSlimes[b]))
                    {
                        _CollidedRed[r] = true;
                        _CollidedBlue[b] = true;
                    }
                }
            
            for (int r = 0; r < MaxSlime; r++)
                for (int b = 0; b < MaxSlime; b++)
                {
                    if ((_RedSlimes[r] == _BlueSlimes[b]) && !_CollidedRed[r] && !_CollidedBlue[b])
                    {
                        CollidedRed[r] = true;
                        CollidedBlue[b] = true;
                    }
                }

            for (int i = 0; i < MaxSlime; i++)
            {
                if (_CollidedRed[i]) CollidedRed[i] = _CollidedRed[i];
                if (_CollidedBlue[i]) CollidedBlue[i] = _CollidedBlue[i];
                LastOrder[Team.Red, i] = order[Team.Red, i];
                LastOrder[Team.Blue, i] = order[Team.Blue, i];
            }

            // 충돌 슬라임의 DeadEvent; 살아남은 슬라임 아래의 타일 색 칠하기
            for (int i = 0; i < MaxSlime; i++)
            {
                if (CollidedRed[i]) DeadEvent(Team.Red, i);
                else RepaintTile(_RedSlimes[i], Team.Red);
                if (CollidedBlue[i]) DeadEvent(Team.Blue, i);
                else RepaintTile(_BlueSlimes[i], Team.Blue);
            }

            // Flipped tiles 계산
            int[,] RedState = GetFlippedTile(Team.Red);
            int[,] BlueState = GetFlippedTile(Team.Blue);

            int[,] NetFlippedTile = new int[Width, Height];

            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    bool r = (RedState[x, y] == 1);
                    bool b = (BlueState[x, y] == 1);
                    int color = -2; // Not Changed
                    if (r) color = Team.Red;
                    if (b) color = Team.Blue;
                    if (r && b) color = Team.Neutral;
                    NetFlippedTile[x, y] = color;
                }

            // 실제로 타일 뒤집기 (BoardState 갱신)
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    int color = NetFlippedTile[x, y];
                    if (color != -2) BoardState[x, y] = color;
                }

            FlipState = NetFlippedTile;

            // 뒤집힌 슬라임의 DeadEvent
            var FlippedSlimes = new bool[Team.Max][];
            var FlippedRed = new bool[MaxSlime]; // all False
            var FlippedBlue = new bool[MaxSlime]; // all False
            FlippedSlimes[Team.Red] = FlippedRed;
            FlippedSlimes[Team.Blue] = FlippedBlue;

            for (int i = 0; i < MaxSlime; i++)
            {
                Point p;
                int color;
                if (!FlippedRed[i])
                {
                    p = _RedSlimes[i];
                    color = NetFlippedTile[p.X, p.Y];
                    if ((Team.Blue == color) || (Team.Neutral == color))
                        FlippedRed[i] = true;
                }
                if (!FlippedBlue[i])
                {
                    p = _BlueSlimes[i];
                    color = NetFlippedTile[p.X, p.Y];
                    if ((Team.Red == color) || (Team.Neutral == color))
                        FlippedBlue[i] = true;
                }
            }
            for (int i = 0; i < MaxSlime; i++)
            {
                if (CollidedRed[i]) DeadEvent(Team.Red, i);
                if (CollidedBlue[i]) DeadEvent(Team.Blue, i);
            }

            // 죽은 슬라임의 Respawn (NextTurn에서 SlimePosition의 마지막 갱신)

            for (int i = 0; i < MaxSlime; i++)
            {
                Point p;
                if (CollidedRed[i] || FlippedRed[i])
                {
                    p = RespawnPoint[Team.Red];
                    SlimePosition[Team.Red][i] = p;
                    BoardState[p.X, p.Y] = Team.Red;
                    TotalDeadSlimes[Team.Red]++;
                }
                else
                    SlimePosition[Team.Red][i] = _SlimePosition[Team.Red][i];
                if (CollidedBlue[i] || FlippedBlue[i])
                {
                    p = RespawnPoint[Team.Blue];
                    SlimePosition[Team.Blue][i] = p;
                    BoardState[p.X, p.Y] = Team.Blue;
                    TotalDeadSlimes[Team.Blue]++;
                }
                else
                    SlimePosition[Team.Blue][i] = _SlimePosition[Team.Blue][i];
            }

            // 점수 갱신
            ScoreUpdate();
            CalculateCurrentWinningTeam();

            CurrentTurn++;
        } // NextTurn

        private void ScoreUpdate()
        {
            for (int iT = 0; iT < Team.Max; iT++)
            {
                Score[iT] = 0;
            }
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    int color = BoardState[x, y];
                    if (color != Team.Neutral)
                        Score[color]++;
                }
            for (int iT = 0; iT < Team.Max; iT++)
            {
                AccScore[iT] += Score[iT];
            }
        }

        private int[,] GetFlippedTile(int team)
        {
            int index = 0;
            Dictionary<int, int> Dict = new Dictionary<int, int>();
            Dict.Add(0, 0);
            int[,] State = new int[Width, Height];
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                    State[x, y] = -1;
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    // 이 타일이 이 팀 타일이 아닌 경우
                    if (BoardState[x, y] != team)
                    {
                        // 바로 아래 타일이 이 팀의 타일인 경우 이 타일에는 새 번호를 부여
                        if ((y == 0) || ((y > 0) && (BoardState[x, y - 1] == team)))
                        {
                            index++;
                            Dict.Add(index, index);
                        }
                        State[x, y] = index;

                        // 바로 왼쪽 타일이 이 팀 타일이 아닌 경우 그 타일 번호를 가져옴 
                        if ((x > 0) && (BoardState[x - 1, y] != team))
                        {
                            int key = State[x - 1, y];
                            int val = GetSmallest(Dict, key);
                            int ival = GetSmallest(Dict, index);
                            if (val <= ival)
                                Dict[ival] = val;
                            else
                            {
                                Dict[val] = ival;
                            }
                        }

                        // 이 타일이 가장자리 부분의 타일인 경우 바깥으로 생각한다.
                        if (IsEdge(x, y))
                        {
                            int key = State[x, y];
                            int val = GetSmallest(Dict, key);
                            Dict[val] = 0;
                        }
                    }
                }
            }

            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    int key = State[x, y];
                    if (-1 != key)
                    {
                        int val = GetSmallest(Dict, key);
                        if (val > 0) State[x, y] = 1;
                        else State[x, y] = 0;
                    }
                }

            return State;
        }

        private static int GetSmallest(Dictionary<int, int> RedDict, int _key)
        {
            int key = _key;
            int val = RedDict[key];
            while (key > val)
            {
                RedDict[key] = RedDict[val];
                key = val;
                val = RedDict[val];
            }
            return val;
        }

        private static bool IsEdge(int x, int y)
        {
            return (x == 0) || (y == 0) || (x == Width - 1) || (y == Height - 1);
        }

        // 다음 위치p를 반환하며 d를 유효하게 갱신한다. (l은 이전 위치)
        public Point NextPosition(Point p, Direction l, ref Direction d)
        {
            if (((int)d < 0) || ((int)d >= 4))
                d = l;

            if (d == Direction.Down) p.Y -= 1;
            else if (d == Direction.Left) p.X -= 1;
            else if (d == Direction.Right) p.X += 1;
            else if (d == Direction.Up) p.Y += 1;

            if (p.X < 0)
            {
                p.X = 1;
                d = Direction.Right;
            }
            else if (p.Y < 0)
            {
                p.Y = 1;
                d = Direction.Up;
            }
            else if (p.X >= Width)
            {
                p.X = Width - 2;
                d = Direction.Left;
            }
            else if (p.Y >= Height)
            {
                p.Y = Height - 2;
                d = Direction.Down;
            }
            return p;
        }

        public void RepaintTile(Point p, int color)
        {
            BoardState[p.X, p.Y] = color;
        }

        public void CalculateCurrentWinningTeam()
        {
            if (Score[Team.Blue] > Score[Team.Red])
                CurrentWinningTeam = Team.Blue;
            else if(Score[Team.Blue] == Score[Team.Red])
            {
                if (TotalDeadSlimes[Team.Blue] < TotalDeadSlimes[Team.Red])
                    CurrentWinningTeam = Team.Blue;
                else if (TotalDeadSlimes[Team.Blue] == TotalDeadSlimes[Team.Red])
                {
                    if (AccScore[Team.Blue] > AccScore[Team.Red])
                        CurrentWinningTeam = Team.Blue;
                    else if (AccScore[Team.Blue] == AccScore[Team.Red])
                    {
                        CurrentWinningTeam = Team.Neutral;
                    }
                    else
                        CurrentWinningTeam = Team.Red;
                }
                else
                    CurrentWinningTeam = Team.Red;
            }
            else
                CurrentWinningTeam = Team.Red;
        }

        public event DeadHandler DeadEvent = delegate { };
    }

    public delegate void DeadHandler(int team, int slimeIndex);

}
