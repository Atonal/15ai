﻿namespace HAJE.SlimeAI.Logic
{
    public class Team
    {
        public const int Red = 0;
        public const int Blue = 1;
        public const int Max = 2;
        public const int Neutral = -1;
    }
}
