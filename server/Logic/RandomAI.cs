﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace HAJE.SlimeAI.Logic
{
    public class RandomAI
    {
        private const int a = 1;//컴파일 체크용ㅋㅋ
        private int team;
        private Point[] myteamLoc;//내 팀에 대해 상대적인 위치 : 만약 오른쪽 팀이라면 좌표를 바꾼다
        private Random r;
        private int[] role;
        private MySlime[] slimes;
        private Point[] target;//상대 위치
        private int turncount = 0;
        public RandomAI(int team)
        {
            this.team = team;
            this.myteamLoc = new Point[Simulation.MaxSlime];
            this.target = new Point[Simulation.MaxSlime];
            this.role = new int[Simulation.MaxSlime];
            //this.previousDir = new  Direction[Simulation.MaxSlime];
            this.r = new Random();
            this.slimes = new MySlime[Simulation.MaxSlime];
            for (int i = 0; i < slimes.Length; i++)
            {
                slimes[i] = new MySlime(team,i);
            }
            

        }

        public Direction[] Run(Point[][] slimePos, int[,] boardState)
        //AI의 main function
        {
            
            Direction[] result = new Direction[Simulation.MaxSlime];
            //return할 것
            MySlime.updateEnemy(slimePos[this.team*(-1)+1]);//0->1, 1->0
            //슬라임들 적의 위치 update
            for (int i = 0; i < slimes.Length; i++)
            {
                myteamLoc[i] = slimePos[team][i];
                slimes[i].updateLoc(myteamLoc[i]);
            }
            //슬라임들 위치 update
            if (turncount < 30)
            {
                for (int i = 0; i < 2; i++)
                {
                    slimes[i].setTarget(convertTeam(new Point(10,5)));
                    slimes[i].setMode(MySlime.attack);
                }
                slimes[2].setTarget(convertTeam(new Point(Simulation.Width-1,Simulation.Height-1)));
                slimes[2].setMode(MySlime.defence);
                slimes[3].setTarget(convertTeam(new Point(Simulation.Width - 1, Simulation.Height - 1)));
                slimes[3].setMode(MySlime.defence);
                slimes[4].setTarget(convertTeam(new Point(Simulation.Width * 2 / 3, Simulation.Height * 2 / 3)));
                slimes[4].setMode(MySlime.attack);
                for (int i = 0; i < 5; i++) result[i] = slimes[i].move();
            }
            else if (turncount < 150)
            {
                bool setSlime = false;
                int closest = 0;
                if (!setSlime)
                {
                    closest = getClosest(convertTeam(new Point(Simulation.Width - 1, Simulation.Height / 2)));
                    slimes[closest].setMode(MySlime.defence);//회피 모드
                    slimes[closest].setTarget(convertTeam(new Point(Simulation.Width - 1, Simulation.Height / 2)));
                    setSlime = true;//이 친구가 죽을때까지 true
                }
                if (slimes[closest].arrive)
                {
                    slimes[closest].setTarget(new Point(10, r.Next((Simulation.Width+1)/2)));
                }
                if (slimes[closest].checkDie())
                {
                    setSlime = false;
                }
                //이 친구는 공격을 한다
                for (int i = 0; i < 3 && i != closest; i++)
                {
                    slimes[i].setMode(MySlime.attack);
                    slimes[i].setTarget(new Point(10, 5));
                }
                for(int i=3;i<result.Length&&i!=closest;i++){
                    slimes[i].setMode(MySlime.attack);
                    slimes[i].setTarget(convertTeam(get1(boardState)[r.Next(get1(boardState).Count)]));
                }
                for (int i = 0; i < result.Length; i++) result[i] = slimes[i].move();
            }
            else
            {
                for (int i = 0; i < result.Length; i++)
                {
                    slimes[i].setTarget(convertTeam(get1(boardState)[r.Next(get1(boardState).Count)]));
                    slimes[i].setMode(MySlime.attack);
                    result[i] = slimes[i].move();
                }
            }
            //슬라임들 타겟을 향해 이동
            
            
            
            //previousDir = result;
            turncount++;
            return result;
        }
        private int[] getDistance(Point xy)
        //어떤 point로부터 아해들까지의 거리
        {
            int[] result = new int[Simulation.MaxSlime];
            for (int i = 0; i < myteamLoc.Length; i++)
            {
                result[i] = Math.Abs(myteamLoc[i].X - xy.X) + Math.Abs(myteamLoc[i].Y - xy.Y);
            }
            return result;
        }
        private int getClosest(Point xy)
        {
            int[] distance = getDistance(xy);
            int min = distance[0];
            int minindex = 0;
            for (int i = 1; i < distance.Length; i++)
            {
                if (distance[i] < min)
                {
                    min = distance[i];
                    minindex = i;
                }
            }
            return minindex;
        }
        private Point convertTeam(Point xy)
        {
            if (this.team == Team.Blue)
            {
                return new Point(Simulation.Width-1 - xy.X, Simulation.Height-1 - xy.Y);
            }
            else return xy;
        }

        private List<Point> get1(int[,] board)
        {
            List<Point> result = new List<Point> { };
            for (int i = 1; i < board.GetLength(0)-1; i++)
            {
                for (int j = 1; j < board.GetLength(1) - 1; j++)
                {
                    if (((board[i, j - 1] == this.team && board[i, j + 1] == this.team)||(board[i-1,j]==this.team&&board[i+1,j]==this.team)&&board[i,j]!=this.team)||board[i,j]==Team.Neutral)
                    {
                        result.Add(new Point(i, j));
                    }
                    
                }
            }
            if (result.Count == 0) result.Add(new Point(10, 5));
            return result;
        }

        private class MySlime
        {
            private int team;
            private static Point[] enemy;//절대 위치
            private static int xy;//전체 이름?
            private int _xy;//이 친구의 번호
            public const int X_first = 0;
            public const int Y_first = 1;

            public const int attack = 0;
            public const int defence = 1;
            public const int draw = 2;
            public const int random = 3;
            public bool arrive;
            public static Direction[] DirList = { Direction.Up, Direction.Left, Direction.Right, Direction.Down };
            private int mode;
            private Point loc;
            private Point target;
            private List<Point> previousloc;
            private Random r;
            public MySlime(int team, int _xy)
            {
                previousloc = new List<Point> { };
                mode = 0;
                this.team = team;
                this.r = new Random();
                this.mode = 2;
                this.arrive = false;
                this._xy = _xy;
            }

            public Direction move()
            //point를 따로 정해 주지 않을 때
            //
            {
                List<Direction> possible;
                Direction result = Direction.Up;
                if (this.mode == MySlime.attack)
                {
                    possible = chaseEnemy();
                    
                    if (possible.Count == 0) result = goPoint(_xy);
                    else result = possible[r.Next(possible.Count)];
                    
                }
                else if (this.mode == MySlime.defence)
                //result가 possible이 아닐 때 회피 모듈 발동!
                //공격 모드일 때는 회피하지 않음
                {
                    possible = evadeEnemy();
                    if (possible.Count == 0)
                    {
                        result = DirList[r.Next(4)];//회피 공간이 없을때는 랜덤하게 피해라
                        
                    }
                    else if (possible.Count == 4)//주변에 적이 없어!
                    {
                        result = goPoint(_xy);
                        
                    }
                    
                }
                else//내정
                {

                }
                if (this.target == this.loc) this.arrive = true;
                else this.arrive = false;
                return result;
            }

            public Direction goPoint(int mode)
            //절대좌표 xy
            //point를 줬을 때, 현재 위치에 따라 어느 방향으로 갈지 정해주는것
            //mode에 따라 어느 축을 우선으로 이동할지 정해주자?
            //사선으로 절대 안감 이건 너무 비효율적이야
            {
                List<Direction> possible = evadeEnemy();
                Direction result;
                if (mode == X_first)
                {
                    if (this.target.X > loc.X) result = Direction.Right;
                    else if (this.target.X < loc.X) result = Direction.Left;
                    else if (this.target.Y > loc.Y) result = Direction.Up;
                    else result = Direction.Down;
                }
                else
                {
                    if (this.target.Y > loc.Y) result = Direction.Up;
                    else if (this.target.Y < loc.Y) result = Direction.Down;
                    else if (this.target.X > loc.X) result = Direction.Right;
                    else result = Direction.Left;
                }
                if (this.target == this.loc) result = DirList[r.Next(DirList.Length)];
                
                return result;
            }
            private Direction reverse(Direction d)
            {
                switch (d)
                {
                    case Direction.Up:
                        return Direction.Down;
                    case Direction.Down:
                        return Direction.Up;
                    case Direction.Left:
                        return Direction.Right;
                    case Direction.Right:
                        return Direction.Left;
                }
                return Direction.Left;
            }
            public static void updateEnemy(Point[] enemy)
            {
                MySlime.enemy = enemy;
            }
            public void updateLoc(Point xy)
            //자신의 행적을 남기면서 현재 자기 위치를 update한다. 항상 턴 시작 때 불러야 됨
            //사실 왜 있는지 모름
            {
                loc = xy;
                previousloc.Add(xy);

            }
            private List<Direction> evadeEnemy()
            //일단 피해!
            {
                List<Direction> possible = new List<Direction> { Direction.Up, Direction.Left, Direction.Right, Direction.Down };
                Point enemy;
                for (int i = 0; i < MySlime.enemy.Length; i++)
                {
                    enemy = MySlime.enemy[i];
                    if (getDistance(enemy) <= 2)
                    {
                        if (enemy.Y > this.loc.Y) possible.Remove(Direction.Up);
                        if (enemy.Y < this.loc.Y) possible.Remove(Direction.Down);
                        if (enemy.X > this.loc.X) possible.Remove(Direction.Right);
                        if (enemy.X < this.loc.X) possible.Remove(Direction.Left);
                    }
                }
                return possible;
            }
            private List<Direction> chaseEnemy()
            //일단 잡아!
            {
                List<Direction> possible = new List<Direction> { };
                Point enemy;
                for (int i = 0; i < MySlime.enemy.Length; i++)
                {
                    enemy = MySlime.enemy[i];
                    if (getDistance(enemy) <= 2)
                    {
                        if (enemy.Y > this.loc.Y) possible.Add(Direction.Up);
                        if (enemy.Y < this.loc.Y) possible.Add(Direction.Down);
                        if (enemy.X > this.loc.X) possible.Add(Direction.Right);
                        if (enemy.X < this.loc.X) possible.Add(Direction.Left);
                    }
                }
                for (int i = 0; i < 4; i++)
                {
                    for (int j = i + 1; j < possible.Count; j++)
                    {
                        if (possible[i] == possible[j]) possible.RemoveAt(j);
                    }
                }
                return possible;
            }
            public void setMode(int i)
            {
                this.mode = i;
            }
            
            private int getDistance(Point xy)
            {
                return Math.Abs(xy.X - this.loc.X) + Math.Abs(xy.Y - this.loc.Y);
            }
            public void setTarget(Point xy)
            {
                this.target = xy;
            }
            public bool checkDie()
            {
                if (getDistance(this.previousloc[previousloc.Count - 1]) != 1) return true;
                return false;
            }
        }
    }
}
