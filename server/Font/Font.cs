﻿using System;
using System.Drawing.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.SlimeAI
{
    public class FontManager
    {
        public PrivateFontCollection fontCollection;

        public FontManager()
        {
            fontCollection = new PrivateFontCollection();

            AddFont("Resource/NanumBarunGothic.ttf");
        }

        public void AddFont(String fontname)
        {
            fontCollection.AddFontFile(fontname);
        }

        public System.Drawing.Font GetFont(int size)
        {
            if (size > 0)
            {
                return new System.Drawing.Font(fontCollection.Families[0], size, System.Drawing.FontStyle.Bold);
            }
            else
            {
                throw new Exception("폰트의 사이즈는 0 이하가 될 수 없습니다.");
            }
        }
    }
}
