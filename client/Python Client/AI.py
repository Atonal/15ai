import random

class AI:
	turn = 0
	team = 0
	BoardState = []
	Locations = []
	turnByte = []
	Width = 25
	Height = 13
	
	def __init__(self):
		self.BoardState = [[None for x in range(self.Height)] for x in range(self.Width)]
		self.Locations = [[None for x in range(2)] for x in range(5)]
		return
	
	def sendmsg(self,s):
		return chr(255) + unicode(s, "utf-8").encode("utf-8")
	
	def setTeam(self,t):
		self.team = t
		return

	def readData(self,data):
		self.turn = ord(data[0])*256+ord(data[1])
		self.turnByte = []
		self.turnByte.append(data[0])
		self.turnByte.append(data[1])
		mod = self.Width*self.Height
		for i in range(mod):
			j=0
			if data[i+2] == 255: j = -1
			else: j = data[i+2]
			self.BoardState[i/self.Height][i%self.Height] = j
		mod+=2
		team = 0
		slimeno = 0
		count = 0
		while count<20:
			self.Locations[slimeno][team] = Point(data[mod+count],data[mod+count+1])
			slimeno+=1
			if slimeno==5: 
				slimeno = 0
				team+=1
			count+=2
		return
	
	def run(self):
		result = []
		for i in range(5):
			result.append(random.randrange(0, 4))
		return result
	
	def writeData(self,d):
		result = self.turnByte[:]
		for direction in d:
			result.append(direction)
		return result
	
	def main(self,data):
		self.readData(data)
		result = self.writeData(self.run())
		for i in range(5):
			result[i+2] = chr(result[i+2])
		return "".join(result)

	def decode(self, bytemsg):
                result = ""
        	for char in bytemsg:
                	result+=chr(char)
                return result



class Direction:
	Up = 1
	Left = 2
	Right = 0
	Down = 3

class Point:
	x = 0
	y = 0
	def __init__(self,locx,locy):
		self.x = locx
		self.y = locy
		return
	
