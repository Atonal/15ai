package org.haje.slimehopscotch.client;

import java.awt.Point;
import java.util.Random;


public class AI {
	int team;
	static int turn;
	public byte[] turnByte;
	Point[][] Locations;
	int[][] BoardState;
	public static final int Width = Main.MAP_WIDTH;
	public static final int Height = Main.MAP_HEIGHT;
	public boolean AllowConnection;
	public AI() {

		Locations = new Point[2][];
		Locations[0] = new Point[5];
		Locations[1] = new Point[5];
		turnByte = new byte[2];
		BoardState = new int[Width][Height];
		AllowConnection = false;
	}
	
	public void setTeam(int team){
		this.team = team;
	}

	private void readData(byte[] data)
    //게임 진행중 턴마다 받을 data
    {
        //read turn
		turn = data[0] & 0xFF;
		turn <<= 8;
		turn |= data[1] & 0xFF;
        turnByte[0] = data[0];
        turnByte[1] = data[1];
        //read board state
        int i;
        for (i = 2; i < Width*Height+2; i++)
        {
            BoardState[(i-2) / Height][(i-2) % Height] = data[i];
            if (data[i] == 255) BoardState[(i - 2) / Height][(i - 2) % Height] = -1;
        }

        //read slime location
        
        int team = 0;
        int slimeno = 0;
        for (; i < Width*Height+22;i+=2 )
        {
        	Locations[team][slimeno] = new Point(data[i], data[i+1]);
            slimeno++;
            if (slimeno == 5)
            {
                slimeno = 0;
                team++;
            }
        }
    }
	private Direction[] run(){
		//슬라임이 어떻게 움직일지를 Direction[]의 형태로 return합니다.
        //메인 코드 : 여기를 수정하세요!
        //기본 버전 : 모든 슬라임이 랜덤하게 움직입니다

        Direction[] result = new Direction[5];
        Random r = new Random();
        Direction[] templist = new Direction[]{Direction.Up,Direction.Left,Direction.Right,Direction.Down};
        for (int i = 0; i < 5; i++)
        {
            result[i] = templist[r.nextInt(4)];
        }
        return result;

	}
	private byte[] writeData(Direction[] d){

        //null의 경우 아래 방향으로 움직임
        {
            byte[] result = new byte[7];
        	result[0] = turnByte[0];
            result[1] = turnByte[1];
            System.out.println("Send data of turn " + turn);

            for (int i = 0; i < 5; i++)
            {
                if (d[i] == Direction.Right) result[i+2] = 0;
                else if (d[i] == Direction.Up) result[i+2] = 1;
                else if (d[i] == Direction.Left) result[i+2] = 2;
                else result[i+2] = 3;
            }
            return result;
        }
	}
	public String decode(byte[] msg){
		//test용 함수
		
		String result = new String(msg,1,msg.length-1);
		return result;
	}
    public byte[] main(byte[] data)
    //서버 or 클라에서 부르는 함수입니다.
    {
        readData(data);
        return writeData(run());
    }    
    
    private enum Direction{
    	Up,Left,Right,Down;
    }

}