﻿using System;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SampleClient
{
    class SocketClient
    {
        public class StateObject
        {
            public Socket workSocket = null;
            public const int BufferSize = 512;
            public byte[] buffer = new byte[BufferSize];
        }


        public class AsynchronousClient
        {
            // The port number for the remote device.
            private const int port = 25252;
            private static byte[] response = new byte[256];


            //Your team name & team number
            private static int teamNumber = 1;
            private static String teamName = "SampleRunningAI";
            private static AI testai = new AI();

            private static void StartClient()
            {
                try
                {
                    IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                    
                    Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                    

                    Send(client, SendTeamName());
                    Receive(client);

                    while (true)
                    {
                        if (testai.AllowConnection)
                        {
                            String message = Console.ReadLine();
                            if (message != null) Send(client, testai.sendmsg(message));
                        }
                        if (AI.turn >= 180) break;
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

            private static byte[] SendTeamName()
            {
                // Convert teamNumber & teamName to byte[] format / teamNumber + teamName -> data[]
                byte[] name = Encoding.UTF8.GetBytes(teamName);
                byte[] data = new byte[24];
                data[0] = (byte)teamNumber;
                testai.setTeam(data[0]);
                for (int i = 0; i < 23; i++)
                {
                    if (i < name.Length) data[i + 1] = name[i];
                    else data[i + 1] = 0;
                }

                return data;

            }

            private static void ConnectCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.
                    Socket client = (Socket)ar.AsyncState;

                    // Complete the connection.
                    client.EndConnect(ar);

                    Console.WriteLine("Socket connected to {0}", client.RemoteEndPoint.ToString());

                    // Signal that the connection has been made.
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void Receive(Socket client)
            {
                try
                {
                    // Create the state object.
                    StateObject state = new StateObject();
                    state.workSocket = client;

                    // Begin receiving the data from the remote device.
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void ReceiveCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the state object and the client socket 
                    // from the asynchronous state object.
                    StateObject state = (StateObject)ar.AsyncState;
                    Socket client = state.workSocket;

                    int bytesRead = client.EndReceive(ar);

                    if (bytesRead > 0)
                    {
                        if (bytesRead < 2)
                        {
                            if (state.buffer[0] == 255)
                            {
                                Console.WriteLine("잘못된 포맷의 데이터를 보냈습니다");
                                return;
                            }
                            Console.WriteLine("당신의 팀 번호는 " + state.buffer[0].ToString() + "입니다");
                            testai.setTeam(state.buffer[0]);
                            testai.AllowConnection = true;
                        }
                        else
                        {
                            SendData(client, state.buffer);
                        }
                    }
                    //state.buffer = new byte[StateObject.BufferSize];
                    Receive(client);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void Send(Socket client, byte[] data)
            {
                // Begin sending the data to the remote device.
                client.BeginSend(data, 0, data.Length, 0, new AsyncCallback(SendCallback), client);
            }

            private static void SendCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.
                    Socket client = (Socket)ar.AsyncState;

                    // Complete sending the data to the remote device.
                    int bytesSent = client.EndSend(ar);
                    Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void SendData(Socket client, byte[] data)
            {
                Send(client, testai.main(data));
            }




            public static void Main(String[] args)
            {
                StartClient();
            }

        }
    }



}